# Hexaskvorky Web - Test environment

The `docker-compose.yml` file in this directory may be used to run a **PostgreSQL** Docker image, with port `5432` exposed for development and local testing purposes.
