# Hexaskvorky Web - Build and execution

## Pre-requisites

### Hexaskvorky Core

To pull the *Hexaskvorky Core* submodule into the working tree, run the following command:

```
$ git submodule update --init --recursive
```

More information on the submodule may be found in the [README.md](./README.md) file.


## Docker

The whole application may be run using 3 Docker images: the *Spring Boot backend*, the *Vue.js frontend*, and *PostgreSQL* as a database server. A default configuration of these images is prepared in a single parameterized `docker-compose.yml` file in the root of this repository.


### Build

The images containing the complete *Hexaskvorky* web application may be built using the following command:

```
$ docker-compose build
```

This creates the application's containers (one for frontend and one for backend), copies all the relevant application sources into them, and builds them, creating the application's Docker images.


### Run

The built images, along with a downloaded *PostgreSQL* image may be run in containers using the following command:

```
$ docker-compose up
```

This will start the Spring Boot application (listening on port `8080`), a web server providing the Vue.js client (listening on port `8081`), and the PostgreSQL database server (to which the backend connects via the Docker networking system - no ports are exposed to the outside).

By default, a default administrator account is created on start-up. The account e-mail is `admin@example.com` and the password is `password`.


### Configuration

The `hexaskvorky-web-backend` docker image has 7 properties configurable via environment variables (`environment` property in `docker-compose.yml`). These may change some of the application's behaviour.

* `HS_JPA_URL`: JDBC URL of the database used
* `HS_JPA_USER` (String): Username for the database connection
* `HS_JPA_PASS` (String): Password for the database connection
* `HS_JWT_SECRET` (String): Secret string for signing of JSON Web Tokens
* `HS_ADM_AUTO` (Boolean): Whether an administrator account is to be automatically created during start-up (if it does not already exist)
* `HS_ADM_EMAIL` (E-mail): Default administrator account e-mail
* `HS_ADM_PASS` (String): Default administrator account password

Default values for these properties may be found inside of the `backend/docker-entrypoint.sh` shell script.


## Local build

### Backend

The backend may be built locally by running the following *Maven* command in the `backend` directory:

```
$ mvn clean package
```

This creates an executable `.jar` file in the `backend/app/hexaskvorky-boot/target` directory.


### Frontend

The frontend dependencies may be downloaded by running the following *NPM* command in the `frontend` directory:

```
$ npm install
```

A development web server on port `8081` with auto-rebuild may then be started by running the following command:

```
$ npm run serve
```

A `dist` directory for deployment of the frontend on a production server may be generated using the following:

```
$ npm run build
```
