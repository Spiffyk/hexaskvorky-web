# Hexaskvorky Web

This is a web version of *Hexaskvorky*, made as a [semestral project for **KIV/PIA**](https://github.com/osvetlik/pia2020/tree/a663be7424b2d9d7c1beac922f6b3b7802d16651/semester-project). *Hexaskvorky* is a modification of *Piškvorky* (aka *five-in-a-row*), taking place in a hexagonal grid.

The following is a brief paper on the project.


## Implementation

### Overview

The web client has been written in **HTML**, **LESS**, and **JavaScript** using the **Vue.js** framework. The application server has been written in **Kotlin** using the **Spring Boot** framework.

The *Hexaskvorky Web* backend resides in a **Maven** *reactor project*, meaning that the application is divided into several Maven modules with *separate concerns*. This way, we can separate APIs from concrete implementations - e.g. the application's APIs are completely independent of the **Java Persistence API**, allowing for a future swapping out of persistence logic for a non-JPA one, should such a change be necessary.

The modules are also separated in such a way that *user management* is independent of *game management* (but not vice versa, for obvious reasons).

The application's modules are additionally grouped into four directories:

* `app` - *Spring Boot* application entry point, configuration classes, and global *application properties*
* `common` - common libraries for *REST* and *JPA* modules
* `game` - game room and lobby management
* `user` - user management and security


### Security

The application uses an authentication mechanism powered by **JSON Web Tokens**. Upon validating user credentials, the server returns a cryptographically signed token containing all the needed authentication information, like user identifier and token expiration time to the client.

Authentication is *stateless*, meaning that the server stores no information about a user session on its side and relies on the client sending their token with each HTTP request or WebSocket handshake in an `Authorization` header of type *Bearer*.

Because the value of the `Authorization` header is not automatically stored and/or sent by web browsers, the server requires no additional CSRF prevention measures, as opposed to the case if *session cookies* were used.


### REST and OpenAPI

The application's controllers are documented using the official **OpenAPI Specification v3** annotations for Kotlin and Java. Upon execution, the **Springdoc** library scans these annotations and generates a JSON file describing the REST API, available at the `/v3/api-docs` endpoint.

The application also exposes a `/swagger-ui` endpoint, containing the **Swagger UI** client application. This client may be used to explore and test the application's REST API.


### Hexaskvorky Core

The backend uses a library called **Hexaskvorky Core**, originally created as a platform-independent engine of the Android version of Hexaskvorky, a semestral project for **KIV/MBKZ**. The library is used by the backend for game logic evaluation. 

*Hexaskvorky Core* is pulled into the repository as a *Git submodule*.


### Game rooms

Each *Hexaskvorky* game takes place in a named *game room*. A room supports up to 4 players (an artificial limit; the engine can actually handle many more). It is allowed, mostly for testing purposes, to play a game in a room with only one player. Users may be invited to play by any of the users present in a room.

There is also a room *owner*, who is the only player allowed to change the room's settings. The room settings include the room name, the amount of players, the size of a row required to win the game, and the room's visibility. If the owner leaves and there are still players present in the room, the ownership is transferred to another player. If a room is empty, it is removed.

A room may be marked *public* by the owner, upon which any online player is able to join the game as long as it is not full and/or running.


### Gameplay

The game is played by clicking on hexagons, which marks them with the colour of the logged in player, provided that it is currently their turn. The board is unlimited - the player can move their view around by holding the left mouse button and dragging around on the game board. All controls are also fully supported on touch devices, like mobile phones.


### Game history

When a game finishes, either by a player winning, or a player leaving, a part of its state is stored in the database. The stored data includes the room name, all participants, the winners (there are more when one of the player leaves), the time of room creation, and the time of the game's end.

The history of games is publicly available to all registered players, even for private rooms. History entries are fetched for the day the rooms were created, and sorted starting by the newest.


## Bonus parts

* Unlimited board
* Password strength evaluation
    * Only rudimentary - the server requires the password to be at least 8 characters long
* Announcements (?)
    * Public games are published via WebSocket
* HTML canvas for the gameplay
* OpenAPI, Swagger, RAML or any other API modeling/specification language with code generation
    * OpenAPI Specification v3 via controller annotations
* Angular, React, any other frontend technology
    * Vue.js used
