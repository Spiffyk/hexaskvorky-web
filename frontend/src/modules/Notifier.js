let id = 0;
function genId() {
    return `${id++}-${Date.now()}`;
}

export default {
    namespaced: true,

    state: () => ({
        notifications: []
    }),

    getters: {
        list(state) {
            return state.notifications;
        },
        any(state) {
            return state.notifications.length > 0;
        }
    },

    mutations: {
        notify(state, notification) {
            if (!notification.level) {
                notification.level = "info";
            }
            state.notifications.push(notification);
        },
        remove(state, id) {
            let index = state.notifications.findIndex(notif => notif.id === id);
            if (index >= 0) {
                state.notifications.splice(index, 1);
            }
        },
        clear(state) {
            state.notifications.splice(0, state.notifications.length);
        }
    },

    actions: {
        notify({ commit }, { level, message }) {
            let id = genId();
            commit("notify", { id, level, message });
            setTimeout(() => commit("remove", id), 5000);
        },
        info({ dispatch }, message) {
            dispatch("notify", { level: "info", message });
        },
        error({ dispatch }, message) {
            dispatch("notify", { level: "error", message });
        },
        warn({ dispatch }, message) {
            dispatch("notify", { level: "warn", message });
        }
    }
};