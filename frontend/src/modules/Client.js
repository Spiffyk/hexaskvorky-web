import {Client as StompClient} from "@stomp/stompjs";

const API_URL = `http://${location.hostname}:8080`; // TODO make configurable
const WS_URL = `ws://${location.hostname}:8080/websocket`;
const AUTH_TOKEN_KEY = "authToken";
const USER_CACHE_KEY = "userCache";
export const LAST_EMAIL_KEY = "lastEmail";

export class ClientError extends Error {
    constructor(message) {
        super(message);
    }
}

const Client = {
    stomp: null
};

let ensureStompQueue = [];

export {Client};

export default {
    namespaced: true,

    state: () => ({
        authToken: null,
        currentUser: null
    }),

    getters: {
        isAuthenticated(state) {
            return !!state.authToken;
        }
    },

    mutations: {
        setAuthToken(state, newToken) {
            if (newToken === null) {
                sessionStorage.removeItem(AUTH_TOKEN_KEY);
            } else {
                sessionStorage.setItem(AUTH_TOKEN_KEY, newToken);
            }
            state.authToken = newToken;
        },

        setCurrentUser(state, user) {
            if (user === null) {
                sessionStorage.removeItem(USER_CACHE_KEY);
            } else {
                sessionStorage.setItem(USER_CACHE_KEY, JSON.stringify(user));
            }
            state.currentUser = user;
        },

        loadAuthData(state) {
            state.authToken = sessionStorage.getItem(AUTH_TOKEN_KEY);
            let userJson = sessionStorage.getItem(USER_CACHE_KEY);
            if (userJson !== null) {
                state.currentUser = JSON.parse(userJson);
            }
        }
    },

    actions: {
        async init({ state, commit, dispatch }) {
            commit("loadAuthData");

            if (state.authToken !== null) {
                let user = await dispatch("selfInfo");
                commit("setCurrentUser", user);
            }
        },

        async apiFetch({ state, dispatch }, { url, method, data }) {
            while (url.startsWith("/")) {
                url = url.substring(1);
            }

            let headers = {};
            if (data !== null && data !== undefined) {
                headers["Content-Type"] = "application/json";
            }
            if (state.authToken) {
                headers["Authorization"] = `Bearer ${state.authToken}`;
            }

            try {
                let response = await fetch(`${API_URL}/${url}`, {
                    method,
                    headers,
                    cache: "no-cache",
                    body: (data !== undefined) ? JSON.stringify(data) : undefined
                });

                if (!response.ok) {
                    if (response.status === 403) {
                        await dispatch("Notifier/warn", `Forbidden error - logging out`, { root: true });
                        await dispatch("logout", { silent: true });
                    }
                }

                return response;
            } catch (e) {
                await dispatch("Notifier/error", `Backend server unreachable`, { root: true });
                await dispatch("logout", { silent: true });
                e.hsLogOnly = true;
                throw e;
            }
        },

        async register({ dispatch }, { email, password, displayName }) {
            let response = await dispatch("apiFetch", {
                url: "/user/register",
                method: "POST",
                data: { email, password, displayName }
            });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            localStorage.setItem(LAST_EMAIL_KEY, data.email);
            await dispatch("Notifier/info", `Registered user '${data.email}'`, { root: true });
            return data;
        },

        async login({ commit, dispatch }, { email, password }) {
            let response = await dispatch("apiFetch", {
                url: "/user/login",
                method: "POST",
                data: { email, password }
            });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            commit("setAuthToken", data.token);
            commit("setCurrentUser", data.user);
            localStorage.setItem(LAST_EMAIL_KEY, data.user.email);
            dispatch("Notifier/info", `Logged in as '${data.user.email}'`, { root: true });
            return await dispatch("ensureStomp", async () => data.user);
        },

        async changePassword({ dispatch }, { oldPassword, newPassword }) {
            let response = await dispatch("apiFetch", {
                url: "/user/change-password",
                method: "POST",
                data: { oldPassword, newPassword }
            });

            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            dispatch("Notifier/info", `Changed password of user '${data.email}'`, { root: true });
            return data;
        },

        async changeEmail({ commit, dispatch }, { password, newEmail }) {
            let response = await dispatch("apiFetch", {
                url: "/user/change-email",
                method: "POST",
                data: { password, newEmail }
            });

            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            commit("setCurrentUser", data);
            dispatch("Notifier/info", `Changed e-mail to '${data.email}'`, { root: true });
            return data;
        },

        async changeDisplayName({ commit, dispatch }, { password, newDisplayName }) {
            let response = await dispatch("apiFetch", {
                url: "/user/change-display-name",
                method: "POST",
                data: { password, newDisplayName }
            });

            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            commit("setCurrentUser", data);
            dispatch("Notifier/info", `Changed display name to '${data.displayName}'`, { root: true });
            return data;
        },

        async selfInfo({ dispatch }) {
            let response = await dispatch("apiFetch", { url: "/user/self" });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            return data;
        },

        async onlineUsers({ dispatch }) {
            let response = await dispatch("apiFetch", { url: "/user/online" });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            return data;
        },

        async publicGames({ dispatch }) {
            let response = await dispatch("apiFetch", { url: "/lobby/games" });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            return data;
        },

        async createGame({ dispatch }) {
            let response = await dispatch("apiFetch", {
                url: "/lobby/create-game",
                method: "POST"
            });
            if (!response.ok) {
                let data = await response.json();
                throw new ClientError(data.message);
            }
        },

        async leaveGame({ dispatch }) {
            let response = await dispatch("apiFetch", {
                url: "/game/leave",
                method: "POST"
            });
            if (!response.ok) {
                let data = await response.json();
                throw new ClientError(data.message);
            }
        },

        async joinGame({ dispatch }, { id }) {
            let response = await dispatch("apiFetch", {
                url: "/lobby/join-game",
                method: "POST",
                data: { "roomId": id }
            });
            if (!response.ok) {
                let data = await response.json();
                if (response.status >= 400 && response.status < 500) {
                    dispatch("Notifier/error", data.message, { root: true });
                    return;
                }
                throw new ClientError(data.message);
            }
        },

        async currentGame({ dispatch }) {
            let response = await dispatch("apiFetch", {
                url: "/game/current"
            });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }
            return data.hasGame ? data.game : null;
        },

        async currentField({ dispatch }) {
            let response = await dispatch("apiFetch", {
                url: "/game/current-field"
            });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }
            return data.field;
        },

        async createInvitation({ dispatch }, { inviteeId }) {
            let response = await dispatch("apiFetch", {
                url: "/lobby/invitation/create",
                method: "POST",
                data: { inviteeId }
            });
            if (!response.ok) {
                let data = await response.json();
                throw new ClientError(data.message);
            }
        },

        async acceptInvitation({ dispatch }) {
            let response = await dispatch("apiFetch", {
                url: "/lobby/invitation/accept",
                method: "POST"
            });
            if (!response.ok) {
                let data = await response.json();
                throw new ClientError(data.message);
            }
        },

        async rejectInvitation({ dispatch }) {
            let response = await dispatch("apiFetch", {
                url: "/lobby/invitation/reject",
                method: "POST"
            });
            if (!response.ok) {
                let data = await response.json();
                throw new ClientError(data.message);
            }
        },

        async friends({ dispatch }) {
            let response = await dispatch("apiFetch", { url: "/friends" });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }
            return data;
        },

        async friendRequests({ dispatch }) {
            let response = await dispatch("apiFetch", { url: "/friends/requests" });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }
            return data;
        },

        async removeFriend({ dispatch }, { friendId }) {
            let response = await dispatch("apiFetch", {
                url: "/friends/remove",
                method: "POST",
                data: { friendId }
            });
            if (!response.ok) {
                let data = await response.json();
                throw new ClientError(data.message);
            }
        },

        async createFriendRequest({ dispatch }, { requesteeId }) {
            let response = await dispatch("apiFetch", {
                url: "/friends/create-request",
                method: "POST",
                data: { requesteeId }
            });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }
            return data;
        },

        async acceptFriendRequest({ dispatch }, { requestorId }) {
            let response = await dispatch("apiFetch", {
                url: "/friends/accept-request",
                method: "POST",
                data: { requestorId }
            });
            if (!response.ok) {
                let data = await response.json();
                throw new ClientError(data.message);
            }
        },

        async rejectFriendRequest({ dispatch }, { requestorId }) {
            let response = await dispatch("apiFetch", {
                url: "/friends/reject-request",
                method: "POST",
                data: { requestorId }
            });
            if (!response.ok) {
                let data = await response.json();
                throw new ClientError(data.message);
            }
        },

        async gameLog({ dispatch }, date) {
            if (date === undefined || date === null) {
                date = "";
            }

            let response = await dispatch("apiFetch", {
                url: `/game-log/${date}`
            });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }
            return data;
        },

        async adminLoadUsers({ dispatch }, page) {
            let response = await dispatch("apiFetch", {
                url: `/admin/user/users?page=${page}`
            });
            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }
            return data;
        },

        async adminChangePassword({ dispatch }, { userId, newPassword }) {
            let response = await dispatch("apiFetch", {
                url: "/admin/user/change-password",
                method: "POST",
                data: { userId, newPassword }
            });

            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            dispatch("Notifier/info", `Changed password of user '${data.email}'`, { root: true });
            return data;
        },

        async adminChangeEmail({ dispatch }, { userId, newEmail }) {
            let response = await dispatch("apiFetch", {
                url: "/admin/user/change-email",
                method: "POST",
                data: { userId, newEmail }
            });

            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            dispatch("Notifier/info", `Changed e-mail to '${data.email}'`, { root: true });
            return data;
        },

        async adminChangeDisplayName({ dispatch }, { userId, newDisplayName }) {
            let response = await dispatch("apiFetch", {
                url: "/admin/user/change-display-name",
                method: "POST",
                data: { userId, newDisplayName }
            });

            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            dispatch("Notifier/info", `Changed display name to '${data.displayName}'`, { root: true });
            return data;
        },

        async adminSetAdmin({ dispatch }, { userId, admin }) {
            let response = await dispatch("apiFetch", {
                url: "/admin/user/set-admin",
                method: "POST",
                data: { userId, admin }
            });

            let data = await response.json();
            if (!response.ok) {
                throw new ClientError(data.message);
            }

            dispatch("Notifier/info", `Changed user role to ${data.isAdmin ? 'Administrator' : 'Regular user'}`, { root: true });
            return data;
        },

        async ping({ dispatch }) {
            let response = await dispatch("apiFetch", { url: "/ping" });
            let data = await response.text();
            if (!response.ok || data !== "OK") {
                throw new ClientError("Ping failed");
            }
        },

        async ensureStomp({ state, dispatch }, func) {
            if (state.authToken === null || (Client.stomp !== null && Client.stomp.connected)) {
                if (typeof func === "function") {
                    return await func();
                }
            }

            return new Promise((resolve) => {
                if (Client.stomp === null) {
                    Client.stomp = new StompClient({
                        brokerURL: WS_URL,
                        connectHeaders: {
                            "Authorization": `Bearer ${state.authToken}`
                        }
                    });
                }

                ensureStompQueue.push({ func, resolve });

                Client.stomp.onConnect = () => {
                    clearInterval(Client.stomp.pingInterval);
                    Client.stomp.pingInterval = setInterval(() => {
                        Client.stomp.publish({destination: "/app/user/ping"});
                    }, 3000);

                    while (ensureStompQueue.length > 0) {
                        let entry = ensureStompQueue.splice(0, 1)[0];
                        if (typeof(entry.func) === "function") {
                            let result = entry.func();
                            if (result instanceof Promise) {
                                result.then(entry.resolve);
                            } else {
                                entry.resolve();
                            }
                        } else {
                            entry.resolve();
                        }
                    }
                };

                Client.stomp.onWebSocketError = (frame) => {
                    console.error(`WS reported an error:`, frame);
                    dispatch("Notifier/error", "Socket error - logging out", { root: true });
                    dispatch("logout", { silent: true });
                };

                Client.stomp.onStompError = (frame) => {
                    console.error(`Broker reported an error:`, frame);
                    dispatch("Notifier/error", "Socket error - logging out", { root: true });
                    dispatch("logout", { silent: true });
                };

                if (!Client.stomp.connected) {
                    Client.stomp.activate();
                }
            });
        },

        async logout({ commit, dispatch }, param) {
            if (param === undefined || param.silent !== true) {
                dispatch("Notifier/info", `Logged out`, { root: true });
            }
            commit("setAuthToken", null);
            commit("setCurrentUser", null);

            if (Client.stomp !== null) {
                clearInterval(Client.stomp.pingInterval);
                if (Client.stomp.connected) {
                    await Client.stomp.publish({destination: "/app/user/disconnect"});
                }
                await Client.stomp.deactivate();
                Client.stomp = null;
            }
        }
    }
};
