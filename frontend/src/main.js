import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
import Client from "@/modules/Client";
import Notifier from "@/modules/Notifier";

Vue.use(Vuex);

let store = new Vuex.Store({
  modules: { Client, Notifier },
  strict: true
});

Vue.config.productionTip = false;
Vue.config.errorHandler = function (err) {
  console.log(err);
  if (!err.hsLogOnly) {
    store.dispatch("Notifier/error", "Unknown error! Please contact the administrator.");
  }
};

new Vue({
  render: h => h(App),
  store
}).$mount('#app');
