package cz.spiffyk.hexaskvorky.web.common

import org.springframework.http.HttpStatus

/**
 * An exception whose message is returned by the REST API.
 */
open class StatusMessageException(
    val httpStatus: HttpStatus,
    override val message: String = "",
    override val cause: Throwable? = null
) : Exception(message, cause)
