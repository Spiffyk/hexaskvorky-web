package cz.spiffyk.hexaskvorky.web.common

import java.time.Instant

data class StatusMessageDto(
    val status: String,
    val statusCode: Int,
    val message: String,
    val timestamp: Instant = Instant.now()
) {
    constructor(e: StatusMessageException) : this(e.httpStatus.reasonPhrase, e.httpStatus.value(), e.message)
}
