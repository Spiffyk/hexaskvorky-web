package cz.spiffyk.hexaskvorky.web.common

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class HexaskvorkyControllerAdvice {

    @ExceptionHandler(StatusMessageException::class)
    fun statusMessageHandler(e: StatusMessageException): ResponseEntity<StatusMessageDto> =
        ResponseEntity.status(e.httpStatus)
            .body(StatusMessageDto(e))

}
