package cz.spiffyk.hexaskvorky.web.common

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Tag(name = "Common")
class CommonController {

    @GetMapping("/ping")
    @Operation(
        summary = "Ping",
        description = "An operation for connection checks. Returns OK for every request."
    )
    fun ping(): String = "OK"

    @GetMapping("/coffee")
    @Operation(
        summary = "Get coffee",
        description = "If at all possible, I'll get you a nice freshly brewed coffee."
    )
    fun coffee() {
        throw StatusMessageException(HttpStatus.I_AM_A_TEAPOT, "Not gonna happen, sorry... :-(")
    }

}
