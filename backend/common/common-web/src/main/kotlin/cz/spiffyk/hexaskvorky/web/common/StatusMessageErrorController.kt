package cz.spiffyk.hexaskvorky.web.common

import io.swagger.v3.oas.annotations.Hidden
import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
@Hidden
class StatusMessageErrorController : ErrorController {

    companion object {
        const val ERROR_PATH = "/error"
    }

    @RequestMapping(ERROR_PATH)
    fun errorPage(request: HttpServletRequest, response: HttpServletResponse): StatusMessageDto {
        return StatusMessageDto(
            status = HttpStatus.resolve(response.status)?.reasonPhrase ?: "Unknown",
            statusCode = response.status,
            message = ""
        )
    }

    override fun getErrorPath(): String = ERROR_PATH
}
