package cz.spiffyk.hexaskvorky.web

import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource

@Configuration
class JpaConfiguration(
    val properties: HexaskvorkyProperties
) {

    @Bean
    fun dataSource(): DataSource = DataSourceBuilder.create()
        .url(properties.jpa.url)
        .username(properties.jpa.username)
        .password(properties.jpa.password)
        .build()

}
