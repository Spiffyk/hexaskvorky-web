package cz.spiffyk.hexaskvorky.web

import cz.spiffyk.hexaskvorky.web.user.security.BearerAuthenticationFilter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Configuration
class SecurityConfiguration(
    val jwtAuthenticationFilter: BearerAuthenticationFilter
) : WebSecurityConfigurerAdapter() {

    companion object {

        /**
         * Matchers of requests for which no authentication nor authorization is required.
         */
        val NO_AUTH_MATCHERS = listOf(
            // System
            "/ping",
            "/coffee",
            "/websocket",
            "/error",
            "/webjars/**",

            // User
            "/user/register",
            "/user/login",

            // Swagger, OAS3
            "/swagger-ui",
            "/swagger-ui/**",
            "/swagger-resources/**",
            "/v3/**",
        ).map(::AntPathRequestMatcher)

        val ADMIN_MATCHERS = listOf(
            "/admin",
            "/admin/**"
        ).map(::AntPathRequestMatcher)
    }

    override fun configure(web: WebSecurity) {
        web.ignoring()
    }

    override fun configure(http: HttpSecurity) {
        http
            .cors()
            .and()
            .authorizeRequests()
            .requestMatchers(*NO_AUTH_MATCHERS.toTypedArray()).permitAll()
            .requestMatchers(*ADMIN_MATCHERS.toTypedArray()).hasRole("ADMIN")
            .anyRequest().authenticated()
            .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and().csrf().disable() // disabling CSRF since we authenticate statelessly via Authorization header
            .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val corsConfiguration = CorsConfiguration().apply {
            allowedOrigins = listOf("*")
            allowedMethods = listOf("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS")
            allowedHeaders = listOf("Authorization", "Content-Type")
        }
        return UrlBasedCorsConfigurationSource().apply {
            registerCorsConfiguration("/**", corsConfiguration)
        }
    }
}
