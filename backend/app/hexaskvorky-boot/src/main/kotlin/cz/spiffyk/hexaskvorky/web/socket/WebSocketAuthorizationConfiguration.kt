package cz.spiffyk.hexaskvorky.web.socket

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer

@Configuration
class WebSocketAuthorizationConfiguration : AbstractSecurityWebSocketMessageBrokerConfigurer() {

    override fun configureInbound(messages: MessageSecurityMetadataSourceRegistry?) {
        messages?.apply {
            anyMessage().authenticated()
        }
    }

    // disabling CSRF since we authenticate statelessly via Authorization header
    override fun sameOriginDisabled(): Boolean = true
}
