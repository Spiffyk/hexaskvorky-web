package cz.spiffyk.hexaskvorky.web

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SwaggerConfiguration {

    companion object {
        const val TOKEN_SECURITY_SCHEME_KEY = "tokenScheme"
    }

    @Bean
    fun openApi(): OpenAPI = OpenAPI()
        .info(
            Info()
                .title("Hexaskvorky")
                .version("1.0")
                .description("The Hexaskvorky backend REST API")
                .license(License().name("GNU GPL v3.0").url("https://www.gnu.org/licenses/gpl-3.0.en.html"))
                .contact(Contact().name("Oto Šťáva").email("oto.stava@gmail.com"))
        )
        .components(
            Components()
                .addSecuritySchemes(
                    TOKEN_SECURITY_SCHEME_KEY,
                    SecurityScheme()
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("bearer")
                        .bearerFormat("JWT")
                )
        )
        .addSecurityItem(SecurityRequirement().addList(TOKEN_SECURITY_SCHEME_KEY))

}
