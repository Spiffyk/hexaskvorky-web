package cz.spiffyk.hexaskvorky.web

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableConfigurationProperties(value = [HexaskvorkyProperties::class])
@ConfigurationPropertiesScan(value = ["cz.spiffyk.hexaskvorky.web"])
@EnableScheduling
@SpringBootApplication
class HexaskvorkyBootApp

fun main(args: Array<String>) {
    runApplication<HexaskvorkyBootApp>(*args)
}
