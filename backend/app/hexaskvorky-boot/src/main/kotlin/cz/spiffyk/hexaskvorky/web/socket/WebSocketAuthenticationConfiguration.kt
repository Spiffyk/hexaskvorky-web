package cz.spiffyk.hexaskvorky.web.socket

import cz.spiffyk.hexaskvorky.web.user.security.BearerAuthenticationChannelInterceptor
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.messaging.simp.config.ChannelRegistration
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE + 99)
class WebSocketAuthenticationConfiguration(
    val bearerAuthenticationChannelInterceptor: BearerAuthenticationChannelInterceptor
) : WebSocketMessageBrokerConfigurer {

    override fun configureClientInboundChannel(registration: ChannelRegistration) {
        registration.interceptors(bearerAuthenticationChannelInterceptor)
    }
}
