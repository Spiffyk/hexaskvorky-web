package cz.spiffyk.hexaskvorky.web

import io.jsonwebtoken.JwtParser
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import java.security.Key

@Configuration
class SecurityBeansConfiguration {

    @Bean
    fun bcryptPasswordEncoder(): PasswordEncoder = BCryptPasswordEncoder()

    @Bean
    fun jwtSecret(): Key = Keys.secretKeyFor(SignatureAlgorithm.HS256)

    @Bean
    fun jwtParser(): JwtParser = Jwts.parserBuilder().setSigningKey(jwtSecret()).build()

}
