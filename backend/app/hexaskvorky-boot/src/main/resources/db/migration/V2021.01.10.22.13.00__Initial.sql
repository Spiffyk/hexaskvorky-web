-- User ----------------------------------------------------------------------------------------------------------------

CREATE TABLE user_acc (
    id              UUID          PRIMARY KEY,
    email           VARCHAR(256)  UNIQUE  NOT NULL,
    password_hash   VARCHAR(256)          NOT NULL,
    display_name    VARCHAR(256)  UNIQUE  NOT NULL
);

CREATE INDEX user_acc_ix_email ON user_acc (email);


CREATE TABLE friends (
    user_a_id   UUID        REFERENCES user_acc (id) ON DELETE CASCADE,
    user_b_id   UUID        REFERENCES user_acc (id) ON DELETE CASCADE,

    PRIMARY KEY  (user_a_id, user_b_id)
);

CREATE INDEX friends_ix_user_a_id ON friends (user_a_id);


-- Game log ------------------------------------------------------------------------------------------------------------

CREATE TABLE game_log (
    id          UUID            PRIMARY KEY,
    name        VARCHAR(256)    NOT NULL,
    created     TIMESTAMPTZ     NOT NULL,
    finished    TIMESTAMPTZ     NOT NULL,
    end_reason  VARCHAR(32)     NOT NULL
);

CREATE INDEX game_log_ix_created ON game_log (created);


CREATE TABLE game_log_player (
    game_id     UUID    NOT NULL    REFERENCES game_log (id)  ON DELETE CASCADE,
    user_id     UUID                REFERENCES user_acc (id),

    PRIMARY KEY  (game_id, user_id)
);

CREATE INDEX game_log_player_ix_user_id ON game_log_player (user_id);


CREATE TABLE game_log_winner (
    game_id     UUID    NOT NULL    REFERENCES game_log (id)  ON DELETE CASCADE,
    user_id     UUID                REFERENCES user_acc (id),

    PRIMARY KEY  (game_id, user_id)
);

CREATE INDEX game_log_winner_ix_user_id ON game_log_winner (user_id);

