package cz.spiffyk.hexaskvorky.web

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

/**
 * Configuration of the Hexaskvorky web application.
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "hexaskvorky")
data class HexaskvorkyProperties(
    /**
     * JPA configuration of Hexaskvorky.
     */
    val jpa: Jpa,

    /**
     * Security configuration of Hexaskvorky.
     */
    val security: Security,

    /**
     * Administration configuration of Hexaskvorky.
     */
    val admin: Admin
) {
    
    data class Jpa(
        /**
         * JDBC database URL.
         */
        val url: String,

        /**
         * Database username.
         */
        val username: String,

        /**
         * Database password
         */
        val password: String
    )

    data class Security(
        /**
         * Validity duration of an authentication token, in minutes.
         */
        val authTokenValidity: Long
    )
    
    data class Admin(
        /**
         * Configuration of a default administrator account.
         */
        val default: Default
    ) {
        data class Default(
            /**
             * Whether a default administrator account should be automatically created if it does not exist.
             */
            val autoCreate: Boolean,

            /**
             * Default administrator account e-mail.
             */
            val email: String,

            /**
             * Default administrator account password.
             */
            val password: String
        )
    }

}
