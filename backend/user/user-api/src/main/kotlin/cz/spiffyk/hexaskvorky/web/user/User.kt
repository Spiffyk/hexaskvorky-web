package cz.spiffyk.hexaskvorky.web.user

import java.util.*

data class User(
    val id: UUID,
    val email: String,
    val passwordHash: String,
    val displayName: String,
    val isAdmin: Boolean
)
