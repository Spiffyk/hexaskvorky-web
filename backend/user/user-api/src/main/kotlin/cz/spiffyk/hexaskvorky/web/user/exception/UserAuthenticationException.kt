package cz.spiffyk.hexaskvorky.web.user.exception

class UserAuthenticationException(
    message: String? = null,
    cause: Throwable? = null
) : UserException(message, cause)
