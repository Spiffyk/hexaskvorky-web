package cz.spiffyk.hexaskvorky.web.user.event

import cz.spiffyk.hexaskvorky.web.user.User
import org.springframework.context.ApplicationEvent

data class FriendsMadeEvent(
    val userA: User,
    val userB: User
) : ApplicationEvent(userA)
