package cz.spiffyk.hexaskvorky.web.user.event

import cz.spiffyk.hexaskvorky.web.user.User
import org.springframework.context.ApplicationEvent

data class FriendsRemovedEvent(
    val userA: User,
    val userB: User
) : ApplicationEvent(userB)
