package cz.spiffyk.hexaskvorky.web.user

import java.util.*
import cz.spiffyk.hexaskvorky.web.user.exception.UserAuthenticationException
import cz.spiffyk.hexaskvorky.web.user.exception.InvalidUserDataException
import org.springframework.data.domain.Pageable
import java.time.Duration
import java.time.temporal.ChronoUnit

/**
 * Service for user management.
 */
interface UserService {

    companion object {
        /**
         * The regular expression an e-mail must always match.
         */
        val EMAIL_REGEX = Regex(""".+@.+""")

        /**
         * The regular expression a user display name must always match.
         */
        val DISPLAY_NAME_REGEX = Regex("""[a-z0-9_]{3,}""")

        /**
         * Minimum password length.
         */
        const val PASSWORD_MIN_CHARS = 8

        /**
         * The duration for which a user is considered online.
         */
        val ONLINE_TIMEOUT = Duration.of(1, ChronoUnit.MINUTES)
    }

    /**
     * Creates a new user with the specified name and password.
     *
     * @throws InvalidUserDataException
     *       if any of the user data is invalid
     */
    fun create(email: String, password: String, displayName: String, isAdmin: Boolean = false): User

    /**
     * Gets all users alphabetically sorted by e-mail, paged by the specified pageable.
     */
    fun getAll(pageable: Pageable): List<User>

    /**
     * Gets the count of all users existing in the application.
     */
    fun countAll(): Long

    /**
     * Gets a user by their ID.
     */
    fun getById(id: UUID): User?

    /**
     * Gets users that match the specified IDs.
     */
    fun getByIds(ids: Iterable<UUID>): List<User>

    /**
     * Gets a user by their e-mail address.
     */
    fun getByEmail(email: String): User?

    /**
     * Checks whether the specified password matches the specified user's password.
     */
    fun checkPassword(user: User, password: String): Boolean

    /**
     * Changes the specified user's password and returns the new user object.
     */
    fun changePassword(user: User, newPassword: String): User

    /**
     * Changes the specified user's e-mail and returns the new user object.
     */
    fun changeEmail(user: User, newEmail: String): User

    /**
     * Changes the specified user's role as admin or regular user and returns the new user object.
     */
    fun setAdmin(user: User, admin: Boolean): User

    /**
     * Changes the specified user's display name and returns the new user object.
     */
    fun changeDisplayName(user: User, newDisplayName: String): User

    /**
     * Generates a new authentication token for the specified user.
     */
    fun generateToken(user: User): String

    /**
     * Attempts to authenticate a user using their e-mail address and password and upon success returns a new
     * authentication token for the user.
     *
     * @throws UserAuthenticationException
     *         if the credentials do not match those of an existing user
     */
    fun generateToken(email: String, password: String): String

    /**
     * Gets the user represented by the specified token.
     *
     * @throws UserAuthenticationException
     *         when the token validation fails
     */
    fun getByToken(token: String): User

    /**
     * Gets a list of all users who are currently online.
     */
    fun getOnline(): List<User>

    /**
     * Marks the specified user ID online as of the current time.
     */
    fun markOnline(id: UUID)

    /**
     * Marks the specified user online as of the current time.
     */
    fun markOnline(user: User) =
        markOnline(user.id)

    /**
     * Marks the specified user ID offline.
     */
    fun markOffline(id: UUID)

    /**
     * Checks whether the specified user ID is currently considered online.
     */
    fun isOnline(id: UUID): Boolean

    /**
     * Creates a friend request from `requestor` to `requestee`. If a friend request in the opposite direction exists,
     * it is automatically accepted.
     */
    fun createFriendRequest(requestor: User, requestee: User): Boolean

    /**
     * Accepts a friend request from `requestor` to `requestee`, if one such exists, creating a friendship between
     * the users.
     *
     * @return `true` if the request existed; `false` otherwise
     */
    fun acceptFriendRequest(requestor: User, requestee: User): Boolean

    /**
     * Removes a friend request from `requestor` to `requestee`, if one such exists.
     */
    fun removeFriendRequest(requestor: User, requestee: User)

    /**
     * Mark the specified users as friends.
     */
    fun makeFriends(userA: User, userB: User)

    /**
     * Unmark the specified users as friends.
     */
    fun removeFriends(userA: User, userB: User)

    /**
     * Gets the specified user's friend requests, in alphabetical order of the requestors' display names.
     */
    fun getFriendRequests(user: User): List<User>

    /**
     * Gets the specified user's friends, in alphabetical order of their display names.
     */
    fun getFriends(user: User): List<User>

    /**
     * Gets the currently authenticated user or `null` if none is authenticated.
     */
    fun getCurrentUser(): User?

}
