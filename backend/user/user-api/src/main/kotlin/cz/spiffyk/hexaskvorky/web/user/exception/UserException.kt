package cz.spiffyk.hexaskvorky.web.user.exception

open class UserException(
    message: String? = null,
    cause: Throwable? = null
) : Exception(message, cause)
