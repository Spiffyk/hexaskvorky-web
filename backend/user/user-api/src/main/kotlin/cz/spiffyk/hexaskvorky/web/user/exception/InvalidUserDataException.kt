package cz.spiffyk.hexaskvorky.web.user.exception

class InvalidUserDataException(override val message: String) : UserException(message)
