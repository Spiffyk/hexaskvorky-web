package cz.spiffyk.hexaskvorky.web.user.event

import cz.spiffyk.hexaskvorky.web.user.User
import org.springframework.context.ApplicationEvent
import java.time.Instant

/**
 * An event signalizing that a user just went offline.
 *
 * @param user
 *      the user who just went offline
 * @param lastActivity
 *      the timestamp of the user's last activity marking them as online
 */
data class UserOfflineEvent(
    val user: User,
    val lastActivity: Instant,
) : ApplicationEvent(user)
