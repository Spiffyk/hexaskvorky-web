package cz.spiffyk.hexaskvorky.web.user.exception

class UserAlreadyExistsException(message: String) : UserException(message) {

    companion object {
        fun ofEmail(email: String) =
            UserAlreadyExistsException("User with e-mail '$email' already exists")

        fun ofDisplayName(displayName: String) =
            UserAlreadyExistsException("User with display name '$displayName' already exists")
    }

}
