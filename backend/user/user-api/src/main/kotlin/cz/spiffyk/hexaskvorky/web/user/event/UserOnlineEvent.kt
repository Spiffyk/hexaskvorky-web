package cz.spiffyk.hexaskvorky.web.user.event

import cz.spiffyk.hexaskvorky.web.user.User
import org.springframework.context.ApplicationEvent
import java.time.Instant

/**
 * An event signalizing that a user just went online.
 *
 * @param user
 *      the user who just went online
 */
data class UserOnlineEvent(
    val user: User,
) : ApplicationEvent(user)
