package cz.spiffyk.hexaskvorky.web.user.event

import cz.spiffyk.hexaskvorky.web.user.User
import org.springframework.context.ApplicationEvent

data class FriendRequestCreatedEvent(
    val requestor: User,
    val requestee: User
) : ApplicationEvent(requestor)
