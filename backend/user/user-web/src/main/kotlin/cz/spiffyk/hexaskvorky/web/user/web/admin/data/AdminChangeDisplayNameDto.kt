package cz.spiffyk.hexaskvorky.web.user.web.admin.data

import java.util.*

data class AdminChangeDisplayNameDto(
    val userId: UUID,
    val newDisplayName: String
)
