package cz.spiffyk.hexaskvorky.web.user.web.admin.data

import java.util.*

data class AdminChangePasswordDto(
    val userId: UUID,
    val newPassword: String
)
