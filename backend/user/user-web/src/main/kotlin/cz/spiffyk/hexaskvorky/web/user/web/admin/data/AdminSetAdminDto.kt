package cz.spiffyk.hexaskvorky.web.user.web.admin.data

import java.util.*

data class AdminSetAdminDto(
    val userId: UUID,
    val admin: Boolean
)
