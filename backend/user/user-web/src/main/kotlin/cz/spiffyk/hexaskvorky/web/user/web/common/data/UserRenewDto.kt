package cz.spiffyk.hexaskvorky.web.user.web.common.data

data class UserRenewDto(
    val token: String
)
