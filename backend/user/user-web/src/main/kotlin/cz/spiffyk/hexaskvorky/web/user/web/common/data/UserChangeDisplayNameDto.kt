package cz.spiffyk.hexaskvorky.web.user.web.common.data

data class UserChangeDisplayNameDto(
    val password: String,
    val newDisplayName: String
)
