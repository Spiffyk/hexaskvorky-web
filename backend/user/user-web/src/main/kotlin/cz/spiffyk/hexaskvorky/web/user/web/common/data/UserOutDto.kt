package cz.spiffyk.hexaskvorky.web.user.web.common.data

import cz.spiffyk.hexaskvorky.web.user.User

data class UserOutDto(val id: String, val displayName: String) {

    companion object {
        fun ofUser(user: User): UserOutDto =
            UserOutDto(
                id = user.id.toString(),
                displayName = user.displayName
            )
    }

}
