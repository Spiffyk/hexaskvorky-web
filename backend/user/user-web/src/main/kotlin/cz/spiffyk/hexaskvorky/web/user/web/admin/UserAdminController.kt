package cz.spiffyk.hexaskvorky.web.user.web.admin

import cz.spiffyk.hexaskvorky.web.common.StatusMessageException
import cz.spiffyk.hexaskvorky.web.user.User
import cz.spiffyk.hexaskvorky.web.user.UserService
import cz.spiffyk.hexaskvorky.web.user.exception.InvalidUserDataException
import cz.spiffyk.hexaskvorky.web.user.exception.UserAlreadyExistsException
import cz.spiffyk.hexaskvorky.web.user.web.admin.data.*
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/admin/user")
@MessageMapping("/admin/user")
@Tag(
    name = "User admin",
    description = "Operations for user administration"
)
class UserAdminController(
    val userService: UserService
) {

    companion object {
        const val PAGE_SIZE = 50
    }

    @GetMapping("/users")
    @Operation(
        summary = "List all users (paged)",
        description = "Lists users, sorted alphabetically by their e-mail addresses. Results are paged, with " +
                "at most $PAGE_SIZE entries on a single page."
    )
    fun getAllUsers(dto: GetAllUsersDto?): GetAllUsersOutDto {
        val page = when {
            (dto === null) -> 0
            (dto.page === null) -> 0
            (dto.page < 0) -> 0
            else -> dto.page
        }

        val totalUsers = userService.countAll()
        val totalPages = run {
            var total = totalUsers / PAGE_SIZE
            if (totalUsers % PAGE_SIZE != 0L) {
                total += 1
            }
            if (total > Int.MAX_VALUE) {
                total = Int.MAX_VALUE.toLong()
            }

            total.toInt()
        }
        val users = userService.getAll(PageRequest.of(page, PAGE_SIZE))

        return GetAllUsersOutDto(
            page = page,
            totalPages = totalPages,
            totalUsers = totalUsers,
            users = users.map(UserPrivilegedOutDto::ofUser)
        )
    }

    @PostMapping("/change-display-name")
    @Operation(
        summary = "Change password",
        description = "Changes the specified user's password to the specified new one"
    )
    fun changeDisplayName(@RequestBody dto: AdminChangeDisplayNameDto): UserPrivilegedOutDto {
        val targetUser = checkAndGetUser(dto.userId)

        try {
            return userService.changeDisplayName(targetUser, dto.newDisplayName)
                .let(UserPrivilegedOutDto::ofUser)
        } catch (e: UserAlreadyExistsException) {
            throw StatusMessageException(
                HttpStatus.CONFLICT,
                "Display name '${dto.newDisplayName}' is already taken"
            )
        } catch (e: InvalidUserDataException) {
            throw StatusMessageException(HttpStatus.BAD_REQUEST, e.message)
        }
    }

    @PostMapping("/change-email")
    @Operation(
        summary = "Change e-mail",
        description = "Changes the specified user's e-mail address to the specified new one"
    )
    fun changeEmail(@RequestBody dto: AdminChangeEmailDto): UserPrivilegedOutDto {
        val targetUser = checkAndGetUser(dto.userId)

        try {
            return userService.changeEmail(targetUser, dto.newEmail)
                .let(UserPrivilegedOutDto::ofUser)
        } catch (e: UserAlreadyExistsException) {
            throw StatusMessageException(
                HttpStatus.CONFLICT,
                "A user with e-mail '${dto.newEmail}' already exists"
            )
        } catch (e: InvalidUserDataException) {
            throw StatusMessageException(HttpStatus.BAD_REQUEST, e.message)
        }
    }

    @PostMapping("/change-password")
    @Operation(
        summary = "Change password",
        description = "Changes the specified user's password to the specified new one"
    )
    fun changePassword(@RequestBody dto: AdminChangePasswordDto): UserPrivilegedOutDto {
        val targetUser = checkAndGetUser(dto.userId)

        try {
            return userService.changePassword(targetUser, dto.newPassword)
                .let(UserPrivilegedOutDto::ofUser)
        } catch (e: InvalidUserDataException) {
            throw StatusMessageException(HttpStatus.BAD_REQUEST, e.message)
        }
    }

    @PostMapping("/set-admin")
    @Operation(
        summary = "Set admin",
        description = "Sets the specified user role to admin or regular user"
    )
    fun setAdmin(@RequestBody dto: AdminSetAdminDto): UserPrivilegedOutDto {
        val targetUser = checkAndGetUser(dto.userId)

        return userService.setAdmin(targetUser, dto.admin)
            .let(UserPrivilegedOutDto::ofUser)
    }

    private fun checkAndGetUser(targetUserId: UUID): User {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        if (targetUserId == user.id) {
            throw StatusMessageException(
                HttpStatus.BAD_REQUEST,
                "Cannot edit the currently logged in user via the administration."
            )
        }

        return userService.getById(targetUserId)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "User with ID $targetUserId not found")
    }

}
