package cz.spiffyk.hexaskvorky.web.user.web.common

import cz.spiffyk.hexaskvorky.web.common.StatusMessageException
import cz.spiffyk.hexaskvorky.web.user.UserService
import cz.spiffyk.hexaskvorky.web.user.exception.InvalidUserDataException
import cz.spiffyk.hexaskvorky.web.user.exception.UserAlreadyExistsException
import cz.spiffyk.hexaskvorky.web.user.exception.UserAuthenticationException
import cz.spiffyk.hexaskvorky.web.user.web.common.data.*
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/user")
@MessageMapping("/user")
@Tag(
    name = "User",
    description = "Operations for user management: registrations, logging in, friend management etc."
)
class UserController(
    val userService: UserService
) {

    @MessageMapping("/ping")
    fun ping() {
        val user = userService.getCurrentUser() ?: throw IllegalStateException("Not authenticated")
        userService.markOnline(user.id)
    }

    @MessageMapping("/disconnect")
    fun disconnect() {
        userService.getCurrentUser()?.also { user ->
            userService.markOffline(user.id)
        }
    }

    @GetMapping("/users/{param}")
    @Operation(summary = "Get user by ID or username")
    fun getByUsernameOrId(@PathVariable param: String): UserOutDto {
        val uuid: UUID
        try {
            uuid = UUID.fromString(param)
        } catch (e: IllegalArgumentException) {
            return userService.getByEmail(param)?.let(UserOutDto::ofUser)
                ?: throw StatusMessageException(HttpStatus.NOT_FOUND, "No user named '$param' has been found")
        }

        return userService.getById(uuid)?.let(UserOutDto::ofUser)
            ?: throw StatusMessageException(HttpStatus.NOT_FOUND, "No user with id $param has been found")
    }

    @GetMapping("/online")
    @Operation(summary = "Get all currently online users")
    fun getOnline(): Collection<UserOutDto> =
        userService.getOnline().map(UserOutDto::ofUser)

    @PostMapping("/register")
    @Operation(summary = "Register user")
    fun register(@RequestBody dto: UserRegisterDto): UserSelfOutDto {
        try {
            return userService.create(dto.email, dto.password, dto.displayName).let(UserSelfOutDto::ofUser)
        } catch (e: UserAlreadyExistsException) {
            throw StatusMessageException(
                HttpStatus.CONFLICT,
                "A user with e-mail '${dto.email}' already exists"
            )
        } catch (e: InvalidUserDataException) {
            throw StatusMessageException(HttpStatus.BAD_REQUEST, e.message)
        }
    }
    
    @PostMapping("/change-display-name")
    @Operation(
        summary = "Change display name",
        description = "Checks the user's password and, if successful, changes their display name to the specified new one"
    )
    fun changeDisplayName(@RequestBody dto: UserChangeDisplayNameDto): UserSelfOutDto {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        if (!userService.checkPassword(user, dto.password)) {
            throw StatusMessageException(HttpStatus.UNAUTHORIZED, "Incorrect password")
        }

        try {
            return userService.changeDisplayName(user, dto.newDisplayName).let(UserSelfOutDto::ofUser)
        } catch (e: UserAlreadyExistsException) {
            throw StatusMessageException(
                HttpStatus.CONFLICT,
                "Display name '${dto.newDisplayName}' is already taken"
            )
        } catch (e: InvalidUserDataException) {
            throw StatusMessageException(HttpStatus.BAD_REQUEST, e.message)
        }
    }

    @PostMapping("/change-email")
    @Operation(
        summary = "Change e-mail",
        description = "Checks the user's password and, if successful, changes their e-mail to the specified new one"
    )
    fun changeEmail(@RequestBody dto: UserChangeEmailDto): UserSelfOutDto {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        if (!userService.checkPassword(user, dto.password)) {
            throw StatusMessageException(HttpStatus.UNAUTHORIZED, "Incorrect password")
        }

        try {
            return userService.changeEmail(user, dto.newEmail).let(UserSelfOutDto::ofUser)
        } catch (e: UserAlreadyExistsException) {
            throw StatusMessageException(
                HttpStatus.CONFLICT,
                "A user with e-mail '${dto.newEmail}' already exists"
            )
        } catch (e: InvalidUserDataException) {
            throw StatusMessageException(HttpStatus.BAD_REQUEST, e.message)
        }
    }

    @PostMapping("/change-password")
    @Operation(
        summary = "Change password",
        description = "Checks the user's old password and, if successful, changes their password to the specified new one"
    )
    fun changePassword(@RequestBody dto: UserChangePasswordDto): UserSelfOutDto {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        if (!userService.checkPassword(user, dto.oldPassword)) {
            throw StatusMessageException(HttpStatus.UNAUTHORIZED, "Old password incorrect")
        }

        try {
            return userService.changePassword(user, dto.newPassword).let(UserSelfOutDto::ofUser)
        } catch (e: InvalidUserDataException) {
            throw StatusMessageException(HttpStatus.BAD_REQUEST, e.message)
        }
    }

    @PostMapping("login")
    @Operation(
        summary = "Login",
        description = "Takes user credentials and, if successful, returns the user's authentication token which may " +
                "subsequently be used as the Authorization Bearer token."
    )
    fun login(@RequestBody dto: UserLoginDto): UserLoginOutDto {
        try {
            val token = userService.generateToken(dto.email, dto.password)
            val user = userService.getByEmail(dto.email)!!
            return UserLoginOutDto(token, UserSelfOutDto.ofUser(user))
        } catch (e: UserAuthenticationException) {
            throw StatusMessageException(
                HttpStatus.UNAUTHORIZED,
                "Incorrect e-mail or password"
            )
        }
    }

    @PostMapping("renew")
    @Operation(
        summary = "Renew token",
        description = "Returns a new authentication token for the currently logged in user"
    )
    fun renew(): UserRenewDto =
        userService.getCurrentUser()
            ?.let(userService::generateToken)
            ?.let(::UserRenewDto)
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")

    @GetMapping("self")
    @Operation(
        summary = "Get current user",
        description = "Gets the current user's information"
    )
    fun self(): UserSelfOutDto = userService.getCurrentUser()?.let(UserSelfOutDto::ofUser)
        ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
}
