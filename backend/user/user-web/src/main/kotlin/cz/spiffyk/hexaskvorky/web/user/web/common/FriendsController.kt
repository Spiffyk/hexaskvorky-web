package cz.spiffyk.hexaskvorky.web.user.web.common

import cz.spiffyk.hexaskvorky.web.common.StatusMessageException
import cz.spiffyk.hexaskvorky.web.user.UserService
import cz.spiffyk.hexaskvorky.web.user.event.FriendRequestCreatedEvent
import cz.spiffyk.hexaskvorky.web.user.event.FriendRequestRemovedEvent
import cz.spiffyk.hexaskvorky.web.user.event.FriendsMadeEvent
import cz.spiffyk.hexaskvorky.web.user.event.FriendsRemovedEvent
import cz.spiffyk.hexaskvorky.web.user.web.common.data.FriendRemoveDto
import cz.spiffyk.hexaskvorky.web.user.web.common.data.FriendRequestCreateDto
import cz.spiffyk.hexaskvorky.web.user.web.common.data.FriendRequestResponseDto
import cz.spiffyk.hexaskvorky.web.user.web.common.data.UserOutDto
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.context.event.EventListener
import org.springframework.http.HttpStatus
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/friends")
@MessageMapping("/friends")
@Tag(
    name = "Friends",
    description = "Operations for friend management: friend requests, removals, listings etc."
)
class FriendsController(
    val userService: UserService,
    val messagingTemplate: SimpMessagingTemplate
) {

    @GetMapping
    @Operation(
        summary = "List friends",
        description = "Lists all friends of the currently logged in user"
    )
    fun listAll(): List<UserOutDto> {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        return userService.getFriends(user).map(UserOutDto::ofUser)
    }

    @GetMapping("/requests")
    @Operation(
        summary = "List friend requests",
        description = "Lists all users who made friend requests to the currently loggeed in user"
    )
    fun listRequests(): List<UserOutDto> {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        return userService.getFriendRequests(user).map(UserOutDto::ofUser)
    }

    @PostMapping("/remove")
    @Operation(
        summary = "Remove friend",
        description = "Removes the specified user from the friend list"
    )
    fun remove(@RequestBody dto: FriendRemoveDto) {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        val friend = userService.getById(dto.friendId)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "User with ID ${dto.friendId} does not exist")
        userService.removeFriends(user, friend)
    }

    @PostMapping("/create-request")
    @Operation(
        summary = "Create friend request",
        description = "Creates a friend request to the specified user"
    )
    fun createRequest(@RequestBody dto: FriendRequestCreateDto): Boolean {
        val requestor = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        if (requestor.id == dto.requesteeId) {
            throw StatusMessageException(HttpStatus.CONFLICT, "You cannot add yourself to your friends list, you weirdo")
        }
        val requestee = userService.getById(dto.requesteeId)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "User with ID ${dto.requesteeId} does not exist")
        return userService.createFriendRequest(requestor, requestee)
    }

    @PostMapping("/accept-request")
    @Operation(
        summary = "Accept friend request",
        description = "Accepts a friend request from the specified user"
    )
    fun acceptRequest(@RequestBody dto: FriendRequestResponseDto) {
        val requestee = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        val requestor = userService.getById(dto.requestorId)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "User with ID ${dto.requestorId} does not exist")
        val existed = userService.acceptFriendRequest(requestor, requestee)
        if (!existed) {
            throw StatusMessageException(HttpStatus.CONFLICT, "No such friend request exists")
        }
    }

    @PostMapping("/reject-request")
    @Operation(
        summary = "Reject friend request",
        description = "Rejects a friend request from the specified user"
    )
    fun rejectRequest(@RequestBody dto: FriendRequestResponseDto) {
        val requestee = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        val requestor = userService.getById(dto.requestorId)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "User with ID ${dto.requestorId} does not exist")
        userService.removeFriendRequest(requestor, requestee)
    }

    @EventListener(FriendsMadeEvent::class)
    fun onFriendsMade(event: FriendsMadeEvent) {
        messagingTemplate.convertAndSendToUser(
            event.userA.id.toString(),
            "/topic/friends/made",
            UserOutDto.ofUser(event.userB)
        )
        messagingTemplate.convertAndSendToUser(
            event.userB.id.toString(),
            "/topic/friends/made",
            UserOutDto.ofUser(event.userA)
        )
    }

    @EventListener(FriendsRemovedEvent::class)
    fun onFriendsRemoved(event: FriendsRemovedEvent) {
        messagingTemplate.convertAndSendToUser(
            event.userA.id.toString(),
            "/topic/friends/removed",
            UserOutDto.ofUser(event.userB)
        )
        messagingTemplate.convertAndSendToUser(
            event.userB.id.toString(),
            "/topic/friends/removed",
            UserOutDto.ofUser(event.userA)
        )
    }

    @EventListener(FriendRequestCreatedEvent::class)
    fun onFriendRequestCreated(event: FriendRequestCreatedEvent) {
        messagingTemplate.convertAndSendToUser(
            event.requestee.id.toString(),
            "/topic/friends/request/created",
            UserOutDto.ofUser(event.requestor)
        )
    }

    @EventListener(FriendRequestRemovedEvent::class)
    fun onFriendRequestRemoved(event: FriendRequestRemovedEvent) {
        messagingTemplate.convertAndSendToUser(
            event.requestee.id.toString(),
            "/topic/friends/request/removed",
            UserOutDto.ofUser(event.requestor)
        )
    }

}
