package cz.spiffyk.hexaskvorky.web.user.web.common.data

data class UserLoginDto(val email: String, val password: String)
