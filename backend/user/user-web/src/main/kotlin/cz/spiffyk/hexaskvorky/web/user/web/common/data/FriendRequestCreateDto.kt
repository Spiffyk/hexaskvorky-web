package cz.spiffyk.hexaskvorky.web.user.web.common.data

import java.util.*

data class FriendRequestCreateDto(val requesteeId: UUID)
