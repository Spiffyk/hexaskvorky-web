package cz.spiffyk.hexaskvorky.web.user.web.common.data

import java.util.*

data class FriendRequestResponseDto(val requestorId: UUID)
