package cz.spiffyk.hexaskvorky.web.user.web.common.data

data class UserChangePasswordDto(
    val oldPassword: String,
    val newPassword: String
)
