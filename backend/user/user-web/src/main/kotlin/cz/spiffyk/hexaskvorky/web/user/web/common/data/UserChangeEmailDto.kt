package cz.spiffyk.hexaskvorky.web.user.web.common.data

data class UserChangeEmailDto(
    val password: String,
    val newEmail: String
)
