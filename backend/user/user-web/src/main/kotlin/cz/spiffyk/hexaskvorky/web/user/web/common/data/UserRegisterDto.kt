package cz.spiffyk.hexaskvorky.web.user.web.common.data

data class UserRegisterDto(val email: String, val password: String, val displayName: String)
