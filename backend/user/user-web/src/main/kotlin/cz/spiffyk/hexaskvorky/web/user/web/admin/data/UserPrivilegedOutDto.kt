package cz.spiffyk.hexaskvorky.web.user.web.admin.data

import cz.spiffyk.hexaskvorky.web.user.User

data class UserPrivilegedOutDto(
    val id: String,
    val email: String,
    val displayName: String,
    val isAdmin: Boolean
) {

    companion object {
        fun ofUser(user: User) = UserPrivilegedOutDto(
            id = user.id.toString(),
            email = user.email,
            displayName = user.displayName,
            isAdmin = user.isAdmin
        )
    }

}
