package cz.spiffyk.hexaskvorky.web.user.web.admin.data

import java.util.*

data class AdminChangeEmailDto(
    val userId: UUID,
    val newEmail: String
)
