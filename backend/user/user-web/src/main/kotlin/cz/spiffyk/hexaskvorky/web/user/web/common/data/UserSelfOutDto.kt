package cz.spiffyk.hexaskvorky.web.user.web.common.data

import cz.spiffyk.hexaskvorky.web.user.User

data class UserSelfOutDto(val id: String, val email: String, val displayName: String, val isAdmin: Boolean) {

    companion object {
        fun ofUser(user: User): UserSelfOutDto =
            UserSelfOutDto(
                id = user.id.toString(),
                email = user.email,
                displayName = user.displayName,
                isAdmin = user.isAdmin
            )
    }

}
