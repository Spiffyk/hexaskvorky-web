package cz.spiffyk.hexaskvorky.web.user.web.common.data

data class UserLoginOutDto(
    val token: String,
    val user: UserSelfOutDto
)
