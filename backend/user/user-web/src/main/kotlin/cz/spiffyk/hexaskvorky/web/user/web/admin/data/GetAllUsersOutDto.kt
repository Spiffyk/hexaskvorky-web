package cz.spiffyk.hexaskvorky.web.user.web.admin.data

data class GetAllUsersOutDto(
    val page: Int,
    val totalPages: Int,
    val totalUsers: Long,
    val users: List<UserPrivilegedOutDto>
)
