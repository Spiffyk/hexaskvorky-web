package cz.spiffyk.hexaskvorky.web.user.web.admin.data

data class GetAllUsersDto(
    val page: Int?
)
