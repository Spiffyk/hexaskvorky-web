package cz.spiffyk.hexaskvorky.web.user

import org.springframework.data.domain.Pageable
import java.util.*

/**
 * DAO for user account objects.
 */
interface UserDao {

    /**
     * Creates a new user with the specified properties.
     */
    fun create(email: String, passwordHash: String, displayName: String, isAdmin: Boolean = false): User

    /**
     * Gets a user by their primary ID.
     */
    fun getById(id: UUID): User?

    /**
     * Gets a list of users by a collection of their primary IDs.
     */
    fun getByIds(ids: Iterable<UUID>): List<User>

    /**
     * Gets a user by their e-mail address.
     */
    fun getByEmail(email: String): User?

    /**
     * Gets a list of all users sorted by their e-mails, paged by the specified pageable.
     */
    fun getAll(pageable: Pageable): List<User>

    /**
     * Gets the count of all users existing in the application.
     */
    fun countAll(): Long

    /**
     * Sets the password hash of the user specified by their primary ID.
     */
    fun setPasswordHash(userId: UUID, newPasswordHash: String): User

    /**
     * Sets the display name of the user specified by their primary ID.
     */
    fun setDisplayName(userId: UUID, newDisplayName: String): User

    /**
     * Sets the e-mail address of the user specified by their primary ID.
     */
    fun setEmail(userId: UUID, newEmail: String): User

    /**
     * Sets the user specified by their primary ID as an admin or regular user.
     */
    fun setAdmin(userId: UUID, admin: Boolean): User

}
