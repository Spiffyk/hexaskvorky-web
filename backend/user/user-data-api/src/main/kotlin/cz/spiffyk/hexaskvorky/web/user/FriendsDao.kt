package cz.spiffyk.hexaskvorky.web.user

/**
 * DAO for inter-user friendships.
 */
interface FriendsDao {

    /**
     * Creates a bi-directional friendship between `userA` and `userB`.
     */
    fun create(userA: User, userB: User): Boolean

    /**
     * Destroys the bi-directional friendship between `userA` and `userB`.
     */
    fun remove(userA: User, userB: User): Boolean

    /**
     * Checks if the specified users are friends.
     */
    fun areFriends(userA: User, userB: User): Boolean

    /**
     * Gets an alphabetically sorted (by display name) list of the specified `user`'s friends.
     */
    fun getFriends(user: User): List<User>

}
