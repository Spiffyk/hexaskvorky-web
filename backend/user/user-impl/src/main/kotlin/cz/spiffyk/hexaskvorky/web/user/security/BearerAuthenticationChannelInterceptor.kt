package cz.spiffyk.hexaskvorky.web.user.security

import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.simp.stomp.StompCommand
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.messaging.support.ChannelInterceptor
import org.springframework.messaging.support.MessageHeaderAccessor
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

/**
 * Interceptor for authentication of WebSockets
 */
@Component
class BearerAuthenticationChannelInterceptor(
    val bearerAuthenticationService: BearerAuthenticationService
) : ChannelInterceptor {

    override fun preSend(message: Message<*>, channel: MessageChannel): Message<*>? {
        val accessor: StompHeaderAccessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor::class.java)
            ?: throw IllegalStateException("Could not find a StompHeaderAccessor!")

        if (StompCommand.CONNECT == accessor.command) {
            val authHeader = accessor.getFirstNativeHeader("Authorization")
            val bearerToken = bearerAuthenticationService.authenticateByHeader(authHeader)
            accessor.user = bearerToken
            SecurityContextHolder.getContext().authentication = bearerToken
        }
        return message
    }
}
