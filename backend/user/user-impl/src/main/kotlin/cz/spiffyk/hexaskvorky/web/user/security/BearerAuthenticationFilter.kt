package cz.spiffyk.hexaskvorky.web.user.security

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class BearerAuthenticationFilter(
    val bearerAuthenticationService: BearerAuthenticationService
) : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val authHeader: String? = request.getHeader("Authorization")
        SecurityContextHolder.getContext().authentication = bearerAuthenticationService.authenticateByHeader(authHeader)
        filterChain.doFilter(request, response)
    }
}
