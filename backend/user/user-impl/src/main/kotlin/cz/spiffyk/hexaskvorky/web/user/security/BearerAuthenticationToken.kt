package cz.spiffyk.hexaskvorky.web.user.security

import cz.spiffyk.hexaskvorky.web.user.User
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class BearerAuthenticationToken(
    val token: String,
    val user: User,
    vararg authorities: GrantedAuthority
) : AbstractAuthenticationToken(authorities.asList()) {

    override fun getCredentials(): String = token

    override fun getPrincipal(): User = user

    override fun isAuthenticated(): Boolean = true

    override fun getName(): String = user.id.toString()
}
