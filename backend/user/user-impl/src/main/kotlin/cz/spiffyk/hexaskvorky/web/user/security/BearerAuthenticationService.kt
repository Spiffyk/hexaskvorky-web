package cz.spiffyk.hexaskvorky.web.user.security

import cz.spiffyk.hexaskvorky.web.user.User
import cz.spiffyk.hexaskvorky.web.user.UserService
import cz.spiffyk.hexaskvorky.web.user.exception.UserAuthenticationException
import mu.KotlinLogging
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Service

@Service
class BearerAuthenticationService(
    val userService: UserService
) {

    private val log = KotlinLogging.logger {}

    companion object {
        const val BEARER_PREFIX = "Bearer "
    }

    /**
     * Gets a [BearerAuthenticationToken] for the specified `Authorization` header value.
     */
    fun authenticateByHeader(authHeader: String?): BearerAuthenticationToken? {
        if (authHeader != null && authHeader.startsWith(BEARER_PREFIX)) {
            val token = authHeader.substring(BEARER_PREFIX.length)
            return authenticateByToken(token)
        }

        return null
    }

    /**
     * Gets a [BearerAuthenticationToken] for the specified JWT token.
     */
    fun authenticateByToken(token: String): BearerAuthenticationToken? {
        try {
            val user = userService.getByToken(token)
            userService.markOnline(user)
            return BearerAuthenticationToken(token, user, *getAuthorities(user))
        } catch (e: UserAuthenticationException) {
            log.debug { "Authentication by token failed: ${e.message}" }
        }

        return null
    }

    private fun getAuthorities(user: User): Array<GrantedAuthority> =
        if (user.isAdmin) {
            arrayOf(SimpleGrantedAuthority("ROLE_USER"), SimpleGrantedAuthority("ROLE_ADMIN"))
        } else {
            arrayOf(SimpleGrantedAuthority("ROLE_USER"))
        }

}
