package cz.spiffyk.hexaskvorky.web.user

import cz.spiffyk.hexaskvorky.web.HexaskvorkyProperties
import cz.spiffyk.hexaskvorky.web.user.event.*
import cz.spiffyk.hexaskvorky.web.user.exception.InvalidUserDataException
import cz.spiffyk.hexaskvorky.web.user.exception.UserAuthenticationException
import cz.spiffyk.hexaskvorky.web.user.security.BearerAuthenticationToken
import io.jsonwebtoken.*
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.security.Key
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import javax.annotation.PostConstruct
import kotlin.collections.HashMap

@Service
class UserServiceImpl(
    val userDao: UserDao,
    val friendsDao: FriendsDao,
    val passwordEncoder: PasswordEncoder,
    val properties: HexaskvorkyProperties,
    @Qualifier("jwtSecret") val jwtSecret: Key,
    val jwtParser: JwtParser,
    val applicationEventPublisher: ApplicationEventPublisher,
    val hexaskvorkyProperties: HexaskvorkyProperties
) : UserService {

    val log = KotlinLogging.logger {}

    /**
     * The timestamp of the last online users sweep.
     */
    var lastSweep: Instant = Instant.now()

    /**
     * The IDs of users who are currently online, and their last activity timestamps.
     */
    val onlineUsers: MutableMap<UUID, Instant> = HashMap()

    /**
     * Active friend requests. The key is the ID of the *requestee*, the value is a set of all *requestors'* IDs.
     */
    val friendRequests: MutableMap<UUID, MutableSet<UUID>> = HashMap()

    @PostConstruct
    fun initializeAdmin() {
        val adminConfig = hexaskvorkyProperties.admin.default
        if (!adminConfig.autoCreate) {
            return
        }
        val defaultAdmin = userDao.getByEmail(adminConfig.email)
        if (defaultAdmin !== null) {
            if (!defaultAdmin.isAdmin) {
                log.warn { "User `${adminConfig.email}` set as a default admin user exists but does not have admin rights" }
            }
            return
        }

        log.info { "Auto-creating default admin user `${adminConfig.email}`" }
        this.create(
            email = adminConfig.email,
            password = adminConfig.password,
            displayName = "admin",
            isAdmin = true
        )
    }

    override fun create(email: String, password: String, displayName: String, isAdmin: Boolean): User =
        userDao.create(
            email = validateEmail(email).toLowerCase(),
            passwordHash = passwordEncoder.encode(validatePassword(password)),
            displayName = validateDisplayName(displayName),
            isAdmin = isAdmin
        )

    override fun getAll(pageable: Pageable): List<User> =
        userDao.getAll(pageable)

    override fun countAll(): Long =
        userDao.countAll()

    override fun getById(id: UUID): User? =
        userDao.getById(id)

    override fun getByIds(ids: Iterable<UUID>): List<User> =
        userDao.getByIds(ids)

    override fun getByEmail(email: String): User? =
        userDao.getByEmail(email.toLowerCase())

    override fun checkPassword(user: User, password: String): Boolean =
        passwordEncoder.matches(password, user.passwordHash)

    override fun changePassword(user: User, newPassword: String): User =
        userDao.setPasswordHash(user.id, passwordEncoder.encode(validatePassword(newPassword)))

    override fun changeDisplayName(user: User, newDisplayName: String): User =
        if (user.displayName == newDisplayName) {
            user
        } else {
            userDao.setDisplayName(user.id, validateDisplayName(newDisplayName))
        }

    override fun changeEmail(user: User, newEmail: String): User =
        if (user.email == newEmail) {
            user
        } else {
            userDao.setEmail(user.id, validateEmail(newEmail).toLowerCase())
        }

    override fun setAdmin(user: User, admin: Boolean): User =
        if (user.isAdmin == admin) {
            user
        } else {
            userDao.setAdmin(user.id, admin)
        }

    override fun generateToken(user: User): String =
        Jwts.builder()
            .setId(UUID.randomUUID().toString())
            .setSubject(user.id.toString())
            .setExpiration(Date.from(Instant.now().plus(properties.security.authTokenValidity, ChronoUnit.MINUTES)))
            .signWith(jwtSecret)
            .compact()

    override fun generateToken(email: String, password: String): String {
        val user = getByEmail(email)
        if (user === null) {
            passwordEncoder.encode("random string to prevent time-based attacks")
            throw UserAuthenticationException()
        }
        if (!checkPassword(user, password)) {
            throw UserAuthenticationException()
        }

        return generateToken(user)
    }

    override fun getByToken(token: String): User {
        try {
            val jwt = jwtParser.parseClaimsJws(token)
            val uuid = jwt.body.subject?.let(UUID::fromString)
                ?: throw UserAuthenticationException("JWT token did not contain a 'sub' claim")
            return getById(uuid) ?: throw UserAuthenticationException("JWT 'sub' claim did not match any existing user")
        } catch (e: JwtException) {
            throw UserAuthenticationException("Could not verify JWT token", e)
        } catch (e: IllegalArgumentException) {
            throw UserAuthenticationException("Malformed 'sub' claim", e)
        }
    }

    override fun getOnline(): List<User> {
        synchronized(onlineUsers) {
            val onlineIds = onlineUsers.entries.asSequence()
                .filter { checkOnline(it.value) }
                .map { it.key }
                .toList()

            return getByIds(onlineIds)
        }
    }

    override fun markOnline(id: UUID) {
        synchronized(onlineUsers) {
            if (!onlineUsers[id].let(this::checkOnline)) {
                val user = getById(id) ?: throw IllegalStateException("A non-existent user cannot be marked as online")
                applicationEventPublisher.publishEvent(UserOnlineEvent(user))
            }
            onlineUsers[id] = Instant.now()
        }
    }

    override fun markOffline(id: UUID) {
        synchronized(onlineUsers) {
            val lastActivity = onlineUsers.remove(id)
            if (lastActivity !== null) {
                val user = getById(id)
                if (user !== null) {
                    applicationEventPublisher.publishEvent(UserOfflineEvent(user, lastActivity))
                }
            }
        }
    }

    override fun isOnline(id: UUID): Boolean {
        synchronized(onlineUsers) {
            return if (onlineUsers[id].let(this::checkOnline)) {
                true
            } else {
                markOffline(id)
                false
            }
        }
    }

    override fun createFriendRequest(requestor: User, requestee: User): Boolean {
        synchronized(friendRequests) {
            if (friendsDao.areFriends(requestor, requestee)) {
                return false
            }

            val requestors = friendRequests.computeIfAbsent(requestee.id) { HashSet() }
            val added = requestors.add(requestor.id)
            if (added) {
                applicationEventPublisher.publishEvent(FriendRequestCreatedEvent(requestor, requestee))
            }
            return added
        }
    }

    override fun acceptFriendRequest(requestor: User, requestee: User): Boolean {
        synchronized(friendRequests) {
            val requestors = friendRequests[requestee.id]
            if (requestors === null || !requestors.contains(requestor.id)) {
                return false
            }
        }

        makeFriends(requestor, requestee)
        return true
    }

    override fun removeFriendRequest(requestor: User, requestee: User) {
        synchronized(friendRequests) {
            val requestors = friendRequests[requestee.id]
            if (requestors === null) {
                return
            }
            val removed = requestors.remove(requestor.id)
            if (requestors.isEmpty()) {
                friendRequests.remove(requestee.id)
            }

            if (removed) {
                applicationEventPublisher.publishEvent(FriendRequestRemovedEvent(requestor, requestee))
            }
        }
    }

    override fun makeFriends(userA: User, userB: User) {
        val created = friendsDao.create(userA, userB)
        synchronized(friendRequests) {
            removeFriendRequest(userA, userB)
            removeFriendRequest(userB, userA)
        }
        if (created) {
            applicationEventPublisher.publishEvent(FriendsMadeEvent(userA, userB))
        }
    }

    override fun removeFriends(userA: User, userB: User) {
        val removed = friendsDao.remove(userA, userB)
        if (removed) {
            applicationEventPublisher.publishEvent(FriendsRemovedEvent(userA, userB))
        }
    }

    override fun getFriendRequests(user: User): List<User> {
        synchronized(friendRequests) {
            val requestors = friendRequests[user.id]
            return if (requestors === null) {
                emptyList()
            } else {
                userDao.getByIds(requestors).sortedBy(User::displayName)
            }
        }
    }

    override fun getFriends(user: User): List<User> =
        friendsDao.getFriends(user)

    override fun getCurrentUser(): User? {
        val authentication = SecurityContextHolder.getContext().authentication
        if (authentication !is BearerAuthenticationToken) {
            return null
        }
        return authentication.user
    }

    /**
     * Sweeps through the online user IDs ([onlineUsers]) and removes all outdated ones.
     */
    @Scheduled(fixedDelay = 10_000)
    fun sweepOnline() {
        synchronized(onlineUsers) {
            val swept = mutableMapOf<UUID, Instant>()
            val it = onlineUsers.iterator()
            while (it.hasNext()) {
                val entry = it.next()
                if (!checkOnline(entry.value)) {
                    swept[entry.key] = entry.value
                    it.remove()
                }
            }

            if (swept.isNotEmpty()) {
                getByIds(swept.keys).forEach { user ->
                    applicationEventPublisher.publishEvent(UserOfflineEvent(user, swept[user.id]!!))
                }
            }

            lastSweep = Instant.now()
        }
    }

    private fun validateEmail(email: String): String {
        if (!UserService.EMAIL_REGEX.matches(email)) {
            throw InvalidUserDataException("An e-mail must contain at least an '@' sign and some characters around it")
        }
        return email
    }

    private fun validateDisplayName(displayName: String): String {
        if (!UserService.DISPLAY_NAME_REGEX.matches(displayName)) {
            throw InvalidUserDataException(
                "A display name must be at least 3 characters long " +
                        "and may only contain lower-case letters, numbers, and underscores"
            )
        }
        return displayName
    }

    private fun validatePassword(password: String): String {
        if (password.length < UserService.PASSWORD_MIN_CHARS) {
            throw InvalidUserDataException("Password must be at least ${UserService.PASSWORD_MIN_CHARS} characters long")
        }
        return password
    }

    /**
     * Checks whether the specified instant of last activity makes a user considered online.
     */
    private fun checkOnline(lastOnline: Instant?): Boolean =
        lastOnline?.let { Duration.between(it, Instant.now()) <= UserService.ONLINE_TIMEOUT }
            ?: false

}
