package cz.spiffyk.hexaskvorky.web.user.jpa

import cz.spiffyk.hexaskvorky.web.user.User
import java.lang.IllegalStateException
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "user_acc")
class UserJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    var id: UUID? = null

    @Column(name = "email")
    lateinit var email: String

    @Column(name = "password_hash")
    lateinit var passwordHash: String

    @Column(name = "display_name")
    lateinit var displayName: String

    @Column(name = "is_admin")
    var isAdmin: Boolean = false


    fun toUser(): User = User(
        id ?: throw IllegalStateException("Cannot convert to User without ID"),
        email,
        passwordHash,
        displayName,
        isAdmin
    )

}
