package cz.spiffyk.hexaskvorky.web.user.jpa

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "friends")
@IdClass(FriendsJpaId::class)
class FriendsJpaEntity {

    @Id
    @Column(name = "user_a_id")
    lateinit var ownerId: UUID

    @Id
    @Column(name = "user_b_id")
    lateinit var friendId: UUID

    @ManyToOne
    @JoinColumn(name = "user_b_id", insertable = false, updatable = false)
    lateinit var friend: UserJpaEntity

}
