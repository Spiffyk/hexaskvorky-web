package cz.spiffyk.hexaskvorky.web.user.jpa

import cz.spiffyk.hexaskvorky.web.user.User
import cz.spiffyk.hexaskvorky.web.user.UserDao
import cz.spiffyk.hexaskvorky.web.user.exception.InvalidUserDataException
import cz.spiffyk.hexaskvorky.web.user.exception.UserAlreadyExistsException
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class UserJpaDao(
    val repository: UserJpaRepository
) : UserDao {

    @Transactional
    override fun create(email: String, passwordHash: String, displayName: String, isAdmin: Boolean): User {
        if (email.length > 256) {
            throw InvalidUserDataException("E-mail may not be longer than 256 characters")
        }
        if (displayName.length > 256) {
            throw InvalidUserDataException("Display name may not be longer than 256 characters")
        }
        if (passwordHash.length > 256) {
            // not throwing InvalidUserDataException because this should never ever happen and the user shouldn't be notified
            throw IllegalArgumentException("Password hash may not be longer than 256 characters")
        }

        if (repository.existsByEmail(email)) {
            throw UserAlreadyExistsException.ofEmail(email)
        }
        if (repository.existsByDisplayName(displayName)) {
            throw UserAlreadyExistsException.ofDisplayName(displayName)
        }

        val entityToSave = UserJpaEntity().apply {
            this.email = email
            this.passwordHash = passwordHash
            this.displayName = displayName
            this.isAdmin = isAdmin
        }
        val savedEntity = repository.saveAndFlush(entityToSave)
        return savedEntity.toUser()
    }

    override fun getByIds(ids: Iterable<UUID>): List<User> =
        repository.findAllById(ids)
            .map(UserJpaEntity::toUser)

    override fun getById(id: UUID): User? =
        repository.findByIdOrNull(id)
            ?.toUser()

    override fun getByEmail(email: String): User? =
        repository.findByEmail(email)
            ?.toUser()

    override fun getAll(pageable: Pageable): List<User> =
        repository.findAllByOrderByEmail(pageable)
            .map(UserJpaEntity::toUser)

    override fun countAll(): Long =
        repository.count()

    override fun setPasswordHash(userId: UUID, newPasswordHash: String): User {
        if (newPasswordHash.length > 256) {
            // not throwing InvalidUserDataException because this should never ever happen and the user shouldn't be notified
            throw IllegalArgumentException("Password hash may not be longer than 256 characters")
        }

        val entity = repository.findByIdOrNull(userId)!!
        entity.passwordHash = newPasswordHash
        val savedEntity = repository.saveAndFlush(entity)
        return savedEntity.toUser()
    }

    @Transactional
    override fun setDisplayName(userId: UUID, newDisplayName: String): User {
        if (newDisplayName.length > 256) {
            throw InvalidUserDataException("Display name may not be longer than 256 characters")
        }

        if (repository.existsByDisplayName(newDisplayName)) {
            throw UserAlreadyExistsException.ofDisplayName(newDisplayName)
        }

        val entity = repository.findByIdOrNull(userId)!!
        entity.displayName = newDisplayName
        val savedEntity = repository.saveAndFlush(entity)
        return savedEntity.toUser()
    }

    @Transactional
    override fun setEmail(userId: UUID, newEmail: String): User {
        if (newEmail.length > 256) {
            throw InvalidUserDataException("E-mail may not be longer than 256 characters")
        }

        if (repository.existsByEmail(newEmail)) {
            throw UserAlreadyExistsException.ofEmail(newEmail)
        }

        val entity = repository.findByIdOrNull(userId)!!
        entity.email = newEmail
        val savedEntity = repository.saveAndFlush(entity)
        return savedEntity.toUser()
    }

    @Transactional
    override fun setAdmin(userId: UUID, admin: Boolean): User {
        val entity = repository.findByIdOrNull(userId)!!
        entity.isAdmin = admin
        val savedEntity = repository.saveAndFlush(entity)
        return savedEntity.toUser()
    }
}
