package cz.spiffyk.hexaskvorky.web.user.jpa

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserJpaRepository : JpaRepository<UserJpaEntity, UUID> {

    fun existsByEmail(email: String): Boolean

    fun existsByDisplayName(displayName: String): Boolean

    fun findByEmail(email: String): UserJpaEntity?

    fun findAllByOrderByEmail(pageable: Pageable): List<UserJpaEntity>

}
