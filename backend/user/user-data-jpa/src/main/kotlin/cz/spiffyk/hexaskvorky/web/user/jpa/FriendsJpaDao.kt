package cz.spiffyk.hexaskvorky.web.user.jpa

import cz.spiffyk.hexaskvorky.web.user.FriendsDao
import cz.spiffyk.hexaskvorky.web.user.User
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class FriendsJpaDao(
    val repository: FriendsJpaRepository
) : FriendsDao {

    @Transactional
    override fun create(userA: User, userB: User): Boolean {
        if (areFriends(userA, userB)) {
            return false
        }

        createDirection(userA, userB)
        createDirection(userB, userA)
        repository.flush()
        return true
    }

    @Transactional
    override fun remove(userA: User, userB: User): Boolean {
        if (!areFriends(userA, userB)) {
            return false
        }

        removeDirection(userA, userB)
        removeDirection(userB, userA)
        repository.flush()
        return true
    }

    override fun areFriends(userA: User, userB: User): Boolean {
        val entityId = FriendsJpaId().apply {
            this.ownerId = userA.id
            this.friendId = userB.id
        }

        return repository.existsById(entityId)
    }

    override fun getFriends(user: User): List<User> =
        repository.findAllByOwnerId(user.id).asSequence()
            .map(FriendsJpaEntity::friend)
            .map(UserJpaEntity::toUser)
            .sortedBy(User::displayName)
            .toList()

    private fun createDirection(userA: User, userB: User) {
        val entityToSave = FriendsJpaEntity().apply {
            this.ownerId = userA.id
            this.friendId = userB.id
        }

        repository.save(entityToSave)
    }

    private fun removeDirection(userA: User, userB: User) {
        val entityId = FriendsJpaId().apply {
            this.ownerId = userA.id
            this.friendId = userB.id
        }

        repository.deleteById(entityId)
    }

}
