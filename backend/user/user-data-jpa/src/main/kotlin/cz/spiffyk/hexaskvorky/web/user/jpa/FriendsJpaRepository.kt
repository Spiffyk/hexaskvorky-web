package cz.spiffyk.hexaskvorky.web.user.jpa

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface FriendsJpaRepository : JpaRepository<FriendsJpaEntity, FriendsJpaId> {

    fun findAllByOwnerId(ownerId: UUID): List<FriendsJpaEntity>

}
