package cz.spiffyk.hexaskvorky.web.user.jpa

import java.io.Serializable
import java.util.*
import javax.persistence.*

class FriendsJpaId : Serializable {

    @Id
    @Column(name = "user_a_id")
    lateinit var ownerId: UUID

    @Id
    @Column(name = "user_b_id")
    lateinit var friendId: UUID

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FriendsJpaId

        if (ownerId != other.ownerId) return false
        if (friendId != other.friendId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = ownerId.hashCode()
        result = 31 * result + friendId.hashCode()
        return result
    }


}
