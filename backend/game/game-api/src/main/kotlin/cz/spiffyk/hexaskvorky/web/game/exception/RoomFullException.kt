package cz.spiffyk.hexaskvorky.web.game.exception

import cz.spiffyk.hexaskvorky.web.game.room.GameRoomInfo

class RoomFullException(
    message: String? = null,
    cause: Throwable? = null
) : GameException(message, cause) {

    constructor(room: GameRoomInfo) : this("Room '${room.name}' is full")
}
