package cz.spiffyk.hexaskvorky.web.game.room

import cz.spiffyk.hexaskvorky.web.user.User
import java.util.*

/**
 * Player slot in a game room.
 */
data class Player(
    /** The user in the player slot. */
    val user: User,

    /** The ID of the slot (for control - a user might technically be in the game twice) */
    val slotId: UUID,

    /** The state of the player */
    var state: PlayerState
)
