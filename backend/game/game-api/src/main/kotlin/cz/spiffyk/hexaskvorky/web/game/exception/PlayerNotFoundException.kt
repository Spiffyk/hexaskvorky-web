package cz.spiffyk.hexaskvorky.web.game.exception

import java.util.*

class PlayerNotFoundException(
    message: String? = null,
    cause: Throwable? = null
) : GameException(message, cause) {

    companion object {
        fun ofSlotId(roomId: UUID, slotId: UUID) = PlayerNotFoundException("Room $roomId has no player slot $slotId")

        fun ofUserId(roomId: UUID, userId: UUID) = PlayerNotFoundException("Room $roomId has no player with user id $userId")
    }

}
