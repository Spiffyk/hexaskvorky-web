package cz.spiffyk.hexaskvorky.web.game.exception

import cz.spiffyk.hexaskvorky.web.user.User

class UserOfflineException(
    message: String? = null,
    cause: Throwable? = null
) {

    constructor(user: User) : this("User '${user.displayName}' is offline")

}
