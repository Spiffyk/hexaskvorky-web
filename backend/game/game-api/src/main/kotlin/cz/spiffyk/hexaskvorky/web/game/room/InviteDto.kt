package cz.spiffyk.hexaskvorky.web.game.room

import java.util.*

data class InviteDto(
    val inviteeId: UUID
)
