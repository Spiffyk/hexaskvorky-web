package cz.spiffyk.hexaskvorky.web.game.exception

open class GameException(
    message: String? = null,
    cause: Throwable? = null
) : Exception(message, cause)
