package cz.spiffyk.hexaskvorky.web.game.event

import cz.spiffyk.hexaskvorky.web.game.room.GameRoomInfo
import cz.spiffyk.hexaskvorky.web.game.room.Player
import cz.spiffyk.hexaskvorky.web.game.room.PlayerState

data class PlayerStateChangedEvent(
    val room: GameRoomInfo,
    val player: Player,
    val oldState: PlayerState,
)
