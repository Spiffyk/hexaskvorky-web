package cz.spiffyk.hexaskvorky.web.game.exception

import cz.spiffyk.hexaskvorky.web.user.User

class UserNotInvitedException(
    message: String? = null,
    cause: Throwable? = null
) : GameException(message, cause) {

    constructor(user: User) : this("User ${user.displayName} is not invited to any game")

}
