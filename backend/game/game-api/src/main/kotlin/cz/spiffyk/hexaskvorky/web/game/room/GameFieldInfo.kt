package cz.spiffyk.hexaskvorky.web.game.room

import cz.spiffyk.hexaskvorky.core.vec.Point2i
import java.util.*

data class GameFieldInfo(
    val slotToInt: Map<UUID, Int>,
    val intToSlot: Map<Int, UUID>,
    val gameField: Map<Point2i, Int>,
    val currentPlayer: Int,
    val winners: List<Int>,
    val winningPoints: List<Point2i>
)
