package cz.spiffyk.hexaskvorky.web.game.event

import cz.spiffyk.hexaskvorky.web.game.room.GameFieldInfo
import cz.spiffyk.hexaskvorky.web.game.room.GameRoomInfo
import org.springframework.context.ApplicationEvent

data class GamePlayedEvent(
    val room: GameRoomInfo,
    val field: GameFieldInfo
) : ApplicationEvent(room)
