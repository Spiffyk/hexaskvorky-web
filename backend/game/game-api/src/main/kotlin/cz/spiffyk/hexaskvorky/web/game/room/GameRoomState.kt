package cz.spiffyk.hexaskvorky.web.game.room

/**
 * The state of a game room.
 */
enum class GameRoomState {
    /** In this state the room is being set up by the players in it. */
    SETTING,

    /** In this state the room is in-game. */
    IN_GAME,

    /**
     * In this state the room is finished.
     */
    FINISHED,

    /** In this state the room is invalid e.g. due to all the players having left it. */
    INVALID,
}
