package cz.spiffyk.hexaskvorky.web.game.exception

import java.util.*

class NotPlayersTurnException(
    message: String? = null,
    cause: Throwable? = null
) : GameException(message, cause) {

    constructor(playerSlotId: UUID) : this("It is not player's $playerSlotId turn")
}
