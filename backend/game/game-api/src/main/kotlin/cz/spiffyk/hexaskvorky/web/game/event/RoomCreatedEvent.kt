package cz.spiffyk.hexaskvorky.web.game.event

import cz.spiffyk.hexaskvorky.web.game.room.GameRoomInfo
import org.springframework.context.ApplicationEvent

data class RoomCreatedEvent(
    val room: GameRoomInfo
) : ApplicationEvent(room)
