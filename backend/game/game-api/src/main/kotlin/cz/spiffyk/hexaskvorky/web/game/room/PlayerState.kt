package cz.spiffyk.hexaskvorky.web.game.room

/**
 * State of a player inside of a [GameRoom].
 */
enum class PlayerState {
    INVITED, NOT_READY, READY
}
