package cz.spiffyk.hexaskvorky.web.game.exception

import cz.spiffyk.hexaskvorky.web.game.room.GameRoomState

class RoomStateException(
    message: String? = null,
    cause: Throwable? = null
) : GameException(message, cause) {

    constructor(expected: GameRoomState, actual: GameRoomState) : this("Room state is not $expected (was $actual)")

}
