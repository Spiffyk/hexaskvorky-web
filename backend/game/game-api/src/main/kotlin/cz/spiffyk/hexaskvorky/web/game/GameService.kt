package cz.spiffyk.hexaskvorky.web.game

import cz.spiffyk.hexaskvorky.web.game.room.*
import cz.spiffyk.hexaskvorky.web.user.User
import java.time.Duration
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.*

interface GameService {

    companion object {
        /**
         * The minimum size of a room.
         */
        const val MIN_ROOM_PLAYERS = 1

        /**
         * The maximum size of a room.
         */
        const val MAX_ROOM_PLAYERS = 4

        /**
         * The minimum `endScore` settable in a room.
         */
        const val MIN_END_SCORE = 3

        /**
         * The maximum `endScore` settable in a room.
         */
        const val MAX_END_SCORE = 8

        val ROOM_SETTING_TIMEOUT = Duration.of(10, ChronoUnit.MINUTES)
    }

    /**
     * Creates a new private game room with default settings and returns its info.
     */
    fun createRoom(owner: User): GameRoomInfo

    /**
     * Gets a list of all public rooms that are currently in the `SETTING` state.
     */
    fun getPublicRooms(): List<GameRoomInfo>

    /**
     * Gets info on the room specified by the provided ID.
     */
    fun getRoomInfo(roomId: UUID): GameRoomInfo?

    fun getFieldInfo(roomId: UUID): GameFieldInfo?

    /**
     * Gets info on the room the specified user is engaged to.
     */
    fun getEngagedRoomInfo(user: User): GameRoomInfo?

    fun getEngagedRoomFieldInfo(user: User): GameFieldInfo?

    /**
     * Disengage the specified user from any room they may be engaged to.
     *
     * @return `true` if the user was disengaged; `false` if the user was not engaged whilst the method was being called
     */
    fun disengageUser(user: User): Boolean

    fun setRoomName(roomId: UUID, name: String): GameRoomInfo

    fun setRoomEndScore(roomId: UUID, endScore: Int): GameRoomInfo

    fun setRoomMaxPlayers(roomId: UUID, maxPlayers: Int): GameRoomInfo

    fun setRoomVisibility(roomId: UUID, visibility: GameRoomVisibility): GameRoomInfo

    /**
     * Adds the specified [user] as a player to the room specified by [roomId].
     *
     * @return the ID of the created player slot
     */
    fun addPlayerToRoom(roomId: UUID, user: User, initialState: PlayerState, inviter: User? = null): UUID

    fun removePlayerFromRoom(roomId: UUID, playerSlotId: UUID): GameRoomInfo

    /**
     * Sets the specified player's state in the room.
     */
    fun setPlayerState(roomId: UUID, playerSlotId: UUID, state: PlayerState): GameRoomInfo

    /**
     * Sets the specified player's state in the room.
     */
    fun setPlayerState(roomId: UUID, user: User, state: PlayerState): GameRoomInfo

    fun acceptInvitation(user: User): GameRoomInfo

    fun rejectInvitation(user: User): GameRoomInfo

    /**
     * Plays a turn in a game specified by [roomId] by a player specified by [playerSlotId], using axial co-ordinates
     * of the game field.
     */
    fun play(roomId: UUID, playerSlotId: UUID, axialX: Int, axialY: Int)

    fun play(roomId: UUID, user: User, axialX: Int, axialY: Int)

    /**
     * Gets all game logs for the specified date.
     */
    fun getGameLogs(date: LocalDate? = null): List<GameLog>

}
