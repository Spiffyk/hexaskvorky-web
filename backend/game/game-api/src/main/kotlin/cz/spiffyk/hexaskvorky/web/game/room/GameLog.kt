package cz.spiffyk.hexaskvorky.web.game.room

import cz.spiffyk.hexaskvorky.web.user.User
import java.time.Instant
import java.util.*

data class GameLog(
    val id: UUID,
    val name: String,
    val created: Instant,
    val finished: Instant,
    val players: Collection<User>,
    val winners: Collection<User>,
    val endReason: GameEndReason
)
