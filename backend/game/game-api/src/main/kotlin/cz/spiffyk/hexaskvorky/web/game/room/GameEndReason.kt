package cz.spiffyk.hexaskvorky.web.game.room

enum class GameEndReason {
    /**
     * The game ended because a player won via normal means, i.e. they got the specified number of hexes in a row.
     */
    PLAYER_WON,

    /**
     * The game ended prematurely because a player left.
     */
    PLAYER_LEFT,
}
