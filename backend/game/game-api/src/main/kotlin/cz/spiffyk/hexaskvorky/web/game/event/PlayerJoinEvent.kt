package cz.spiffyk.hexaskvorky.web.game.event

import cz.spiffyk.hexaskvorky.web.game.room.GameRoomInfo
import cz.spiffyk.hexaskvorky.web.game.room.PlayerState
import cz.spiffyk.hexaskvorky.web.user.User
import org.springframework.context.ApplicationEvent

data class PlayerJoinEvent(
    val user: User,
    val room: GameRoomInfo,
    val playerState: PlayerState,
    val inviter: User?
) : ApplicationEvent(room)
