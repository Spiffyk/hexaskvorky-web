package cz.spiffyk.hexaskvorky.web.game.room

import cz.spiffyk.hexaskvorky.web.user.User
import java.time.Instant
import java.util.*

data class GameRoomInfo(
    val id: UUID,
    val created: Instant,
    val name: String,
    val owner: User,
    val maxPlayers: Int,
    val players: Collection<Player>,
    val state: GameRoomState,
    val visibility: GameRoomVisibility,
    val endScore: Int,
)
