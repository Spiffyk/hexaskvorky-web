package cz.spiffyk.hexaskvorky.web.game.exception

import cz.spiffyk.hexaskvorky.web.user.User
import java.util.*

class UserEngagedException(
    message: String? = null,
    cause: Throwable? = null
) : GameException(message, cause) {

    constructor(user: User) : this("User '${user.displayName}' is already engaged to a game")

}
