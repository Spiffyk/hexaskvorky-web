package cz.spiffyk.hexaskvorky.web.game.exception

import java.util.*

class RoomNotFoundException(
    message: String? = null,
    cause: Throwable? = null
) : GameException(message, cause) {

    constructor(roomId: UUID) : this("Could not find a room with id $roomId")

}
