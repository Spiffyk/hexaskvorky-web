package cz.spiffyk.hexaskvorky.web.game.room

enum class GameRoomVisibility {
    PUBLIC, PRIVATE
}
