package cz.spiffyk.hexaskvorky.web.game.event

import cz.spiffyk.hexaskvorky.web.game.room.GameFieldInfo
import cz.spiffyk.hexaskvorky.web.game.room.GameRoomInfo
import cz.spiffyk.hexaskvorky.web.user.User
import org.springframework.context.ApplicationEvent

data class PlayerLeaveEvent(
    val user: User,
    val room: GameRoomInfo,
    val field: GameFieldInfo? = null
) : ApplicationEvent(room)
