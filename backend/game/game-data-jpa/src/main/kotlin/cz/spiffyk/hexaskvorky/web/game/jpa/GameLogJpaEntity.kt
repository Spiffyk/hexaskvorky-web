package cz.spiffyk.hexaskvorky.web.game.jpa

import cz.spiffyk.hexaskvorky.web.game.room.GameEndReason
import cz.spiffyk.hexaskvorky.web.game.room.GameLog
import cz.spiffyk.hexaskvorky.web.user.jpa.UserJpaEntity
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "game_log")
class GameLogJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    var id: UUID? = null

    @Column(name = "name")
    lateinit var name: String

    @Column(name = "created")
    lateinit var created: Instant

    @Column(name = "finished")
    lateinit var finished: Instant

    @ManyToMany
    @JoinTable(
        name = "game_log_player",
        joinColumns = [JoinColumn(name = "game_id")],
        inverseJoinColumns = [JoinColumn(name = "user_id")]
    )
    lateinit var players: List<UserJpaEntity>

    @ManyToMany
    @JoinTable(
        name = "game_log_winner",
        joinColumns = [JoinColumn(name = "game_id")],
        inverseJoinColumns = [JoinColumn(name = "user_id")]
    )
    lateinit var winners: List<UserJpaEntity>

    @Enumerated(value = EnumType.STRING)
    @Column(name = "end_reason")
    lateinit var endReason: GameEndReason


    fun toGameLog(): GameLog =
        GameLog(
            id = id ?: throw IllegalStateException("Cannot convert to GameLog without ID"),
            name = name,
            created = created,
            finished = finished,
            players = players.map(UserJpaEntity::toUser),
            winners = winners.map(UserJpaEntity::toUser),
            endReason = endReason
        )
}
