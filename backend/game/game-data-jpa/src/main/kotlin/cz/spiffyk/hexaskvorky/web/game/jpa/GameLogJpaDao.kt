package cz.spiffyk.hexaskvorky.web.game.jpa

import cz.spiffyk.hexaskvorky.web.game.GameLogDao
import cz.spiffyk.hexaskvorky.web.game.room.GameEndReason
import cz.spiffyk.hexaskvorky.web.game.room.GameLog
import cz.spiffyk.hexaskvorky.web.user.User
import cz.spiffyk.hexaskvorky.web.user.jpa.UserJpaRepository
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneOffset
import javax.transaction.Transactional

@Service
class GameLogJpaDao(
    val gameLogRepository: GameLogJpaRepository,
    val userRepository: UserJpaRepository
) : GameLogDao {

    @Transactional
    override fun create(
        name: String,
        created: Instant,
        finished: Instant,
        players: Collection<User>,
        winners: Collection<User>,
        endReason: GameEndReason
    ): GameLog {
        val entityToSave = GameLogJpaEntity().apply {
            this.name = name
            this.created = created
            this.finished = finished
            this.players = userRepository.findAllById(players.map(User::id))
            this.winners = userRepository.findAllById(winners.map(User::id))
            this.endReason = endReason
        }

        val savedEntity = gameLogRepository.saveAndFlush(entityToSave)
        return savedEntity.toGameLog()
    }

    override fun getAllByCreatedDate(date: LocalDate): List<GameLog> {
        val start = date.atStartOfDay().toInstant(ZoneOffset.UTC)
        val end = date.plusDays(1).atStartOfDay().toInstant(ZoneOffset.UTC)
        val entities = gameLogRepository.findAllByCreatedBetweenOrderByCreatedDesc(start, end)
        return entities.map(GameLogJpaEntity::toGameLog)
    }
}
