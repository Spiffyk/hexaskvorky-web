package cz.spiffyk.hexaskvorky.web.game.jpa

import org.springframework.data.jpa.repository.JpaRepository
import java.time.Instant
import java.util.*

interface GameLogJpaRepository : JpaRepository<GameLogJpaEntity, UUID> {

    fun findAllByCreatedBetweenOrderByCreatedDesc(start: Instant, end: Instant): List<GameLogJpaEntity>

}
