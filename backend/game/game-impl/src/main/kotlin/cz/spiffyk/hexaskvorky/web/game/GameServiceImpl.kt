package cz.spiffyk.hexaskvorky.web.game

import cz.spiffyk.hexaskvorky.core.vec.Point2i
import cz.spiffyk.hexaskvorky.web.game.event.*
import cz.spiffyk.hexaskvorky.web.game.exception.*
import cz.spiffyk.hexaskvorky.web.game.room.*
import cz.spiffyk.hexaskvorky.web.user.User
import cz.spiffyk.hexaskvorky.web.user.UserService
import cz.spiffyk.hexaskvorky.web.user.event.UserOfflineEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.Duration
import java.time.Instant
import java.time.LocalDate
import java.util.*

@Service
class GameServiceImpl(
    val applicationEventPublisher: ApplicationEventPublisher,
    val userService: UserService,
    val gameLogDao: GameLogDao
) : GameService {

    private val rooms: MutableMap<UUID, GameRoom> = mutableMapOf()

    /**
     * Maps users' IDs to IDs of the game rooms they are engaged in.
     */
    private val engagedUsers: MutableMap<UUID, UUID> = mutableMapOf()


    override fun createRoom(owner: User): GameRoomInfo {
        synchronized(rooms) {
            val room = GameRoom(
                id = generateRoomId(),
                created = Instant.now(),
                owner = owner,
                name = "Unnamed room",
                endScore = 5,
                maxPlayers = 2,
                visibility = GameRoomVisibility.PRIVATE,
            )

            rooms[room.id] = room
            val roomInfo = room.toRoomInfo()
            applicationEventPublisher.publishEvent(RoomCreatedEvent(roomInfo))
            return roomInfo
        }
    }

    override fun getPublicRooms(): List<GameRoomInfo> {
        synchronized(rooms) {
            return rooms.values.asSequence()
                .filter { it.visibility == GameRoomVisibility.PUBLIC }
                .filter { it.state == GameRoomState.SETTING }
                .map(GameRoom::toRoomInfo)
                .toList()
        }
    }

    override fun getRoomInfo(roomId: UUID): GameRoomInfo? {
        synchronized(rooms) {
            return rooms[roomId]?.let(GameRoom::toRoomInfo)
        }
    }

    override fun getFieldInfo(roomId: UUID): GameFieldInfo? {
        synchronized(rooms) {
            return rooms[roomId]?.let(GameRoom::toFieldInfo)
        }
    }

    override fun getEngagedRoomInfo(user: User): GameRoomInfo? {
        synchronized(engagedUsers) {
            return engagedUsers[user.id]?.let(this::getRoomInfo)
        }
    }

    override fun getEngagedRoomFieldInfo(user: User): GameFieldInfo? {
        synchronized(engagedUsers) {
            return engagedUsers[user.id]?.let(this::getFieldInfo)
        }
    }

    override fun disengageUser(user: User): Boolean {
        synchronized(engagedUsers) {
            val roomId = engagedUsers[user.id]
            if (roomId === null) {
                return false
            }
            val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
            val slotId = room.players.asSequence()
                .filter { it.value.user.id == user.id }
                .map { it.key }
                .first()
            removePlayerFromRoom(roomId, slotId)
            return true
        }
    }

    override fun setRoomName(roomId: UUID, name: String): GameRoomInfo =
        changeRoomSettings(roomId) { it.name = name }

    override fun setRoomEndScore(roomId: UUID, endScore: Int): GameRoomInfo =
        changeRoomSettings(roomId) { it.endScore = endScore }

    override fun setRoomMaxPlayers(roomId: UUID, maxPlayers: Int): GameRoomInfo =
        changeRoomSettings(roomId) { it.maxPlayers = maxPlayers }

    override fun setRoomVisibility(roomId: UUID, visibility: GameRoomVisibility): GameRoomInfo =
        changeRoomSettings(roomId) { it.visibility = visibility }

    override fun addPlayerToRoom(roomId: UUID, user: User, initialState: PlayerState, inviter: User?): UUID {
        synchronized(rooms) {
            synchronized(engagedUsers) {
                if (engagedUsers.containsKey(user.id)) {
                    throw UserEngagedException(user)
                }

                val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
                val oldInfo = room.toRoomInfo()
                room.unreadyPlayers()
                val slotId = room.add(user, initialState)
                engagedUsers[user.id] = room.id

                val currentInfo = room.toRoomInfo()
                applicationEventPublisher.publishEvent(
                    RoomSettingsChangedEvent(
                        room = currentInfo,
                        oldRoom = oldInfo
                    )
                )
                applicationEventPublisher.publishEvent(
                    PlayerJoinEvent(
                        user = user,
                        room = currentInfo,
                        playerState = initialState,
                        inviter = inviter
                    )
                )
                return slotId

            }
        }
    }

    override fun removePlayerFromRoom(roomId: UUID, playerSlotId: UUID): GameRoomInfo {
        synchronized(rooms) {
            val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
            synchronized(engagedUsers) {
                val oldInfo = room.toRoomInfo()
                room.unreadyPlayers()
                val userId = room.removeBySlotId(playerSlotId)
                engagedUsers.remove(userId)
                val user = userService.getById(userId)!!
                val roomInfo = room.toRoomInfo()
                applicationEventPublisher.publishEvent(
                    PlayerLeaveEvent(
                        user = user,
                        room = roomInfo,
                        field = if (room.state == GameRoomState.FINISHED) {
                            room.toFieldInfo()
                        } else {
                            null
                        }
                    )
                )
                if (room.players.isEmpty()) {
                    rooms.remove(roomId)
                    disposeRoom(room)
                } else {
                    applicationEventPublisher.publishEvent(
                        RoomSettingsChangedEvent(
                            room = roomInfo,
                            oldRoom = oldInfo
                        )
                    )

                    if (roomInfo.state == GameRoomState.FINISHED && roomInfo.state != oldInfo.state) {
                        logFinishedGame(room, GameEndReason.PLAYER_LEFT)
                    }
                }
                return roomInfo
            }
        }
    }

    override fun setPlayerState(roomId: UUID, playerSlotId: UUID, state: PlayerState): GameRoomInfo {
        synchronized(rooms) {
            val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
            val oldInfo = room.toRoomInfo()
            room.setPlayerStateBySlotId(playerSlotId, state)
            val currentInfo = room.toRoomInfo()
            applicationEventPublisher.publishEvent(
                RoomSettingsChangedEvent(
                    room = currentInfo,
                    oldRoom = oldInfo
                )
            )
            return currentInfo
        }
    }

    override fun setPlayerState(roomId: UUID, user: User, state: PlayerState): GameRoomInfo {
        synchronized(rooms) {
            val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
            val slotId = room.players.values.asSequence()
                .filter { it.user.id == user.id }
                .map(Player::slotId)
                .first()
            return setPlayerState(roomId, slotId, state)
        }
    }

    override fun acceptInvitation(user: User): GameRoomInfo {
        synchronized(engagedUsers) {
            val roomId = engagedUsers[user.id] ?: throw UserNotInvitedException(user)
            synchronized(rooms) {
                val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
                val player = room.players.values.asSequence()
                    .filter { it.user.id == user.id }
                    .first()
                if (player.state != PlayerState.INVITED) {
                    throw UserNotInvitedException(user)
                }
                return setPlayerState(roomId, player.slotId, PlayerState.NOT_READY)
            }
        }
    }

    override fun rejectInvitation(user: User): GameRoomInfo {
        synchronized(engagedUsers) {
            val roomId = engagedUsers[user.id] ?: throw UserNotInvitedException(user)
            synchronized(rooms) {
                val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
                val player = room.players.values.asSequence()
                    .filter { it.user.id == user.id }
                    .first()
                if (player.state != PlayerState.INVITED) {
                    throw UserNotInvitedException(user)
                }
                return removePlayerFromRoom(room.id, player.slotId)
            }
        }
    }

    override fun play(roomId: UUID, playerSlotId: UUID, axialX: Int, axialY: Int) {
        synchronized(rooms) {
            val room = rooms[roomId]
                ?: throw RoomNotFoundException(roomId)
            val oldInfo = room.toRoomInfo()
            val point = Point2i.of(axialX, axialY)
            room.play(playerSlotId, point)
            val currentInfo = room.toRoomInfo()

            val gameField = mutableMapOf<Point2i, Int>()
            room.game.forEachFilledHex(gameField::put)
            applicationEventPublisher.publishEvent(GamePlayedEvent(room.toRoomInfo(), room.toFieldInfo()))

            if (oldInfo != currentInfo) {
                applicationEventPublisher.publishEvent(
                    RoomSettingsChangedEvent(
                        room = currentInfo,
                        oldRoom = oldInfo
                    )
                )

                if (room.state == GameRoomState.FINISHED) {
                    logFinishedGame(room, GameEndReason.PLAYER_WON)
                }
            }
        }
    }

    override fun play(roomId: UUID, user: User, axialX: Int, axialY: Int) {
        synchronized(rooms) {
            val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
            val slotId = room.players.values.asSequence()
                .filter { it.user.id == user.id }
                .map(Player::slotId)
                .first()
            play(roomId, slotId, axialX, axialY)
        }
    }

    override fun getGameLogs(date: LocalDate?): List<GameLog> =
        gameLogDao.getAllByCreatedDate(date ?: LocalDate.now())

    @EventListener(UserOfflineEvent::class)
    fun onUserOffline(event: UserOfflineEvent) {
        synchronized(engagedUsers) {
            val roomId = engagedUsers.remove(event.user.id)
            if (roomId !== null) {
                synchronized(rooms) {
                    val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
                    val oldInfo = room.toRoomInfo()
                    room.removeByUserId(event.user.id)
                    val currentInfo = room.toRoomInfo()
                    applicationEventPublisher.publishEvent(
                        RoomSettingsChangedEvent(
                            room = currentInfo,
                            oldRoom = oldInfo
                        )
                    )
                }
            }
        }
    }

    @Scheduled(fixedDelay = 30_000)
    fun sweepRooms() {
        synchronized(rooms) {
            val it = rooms.iterator()
            while (it.hasNext()) {
                val entry = it.next()
                val room = entry.value

                if (isRoomStale(room)) {
                    it.remove()
                    disposeRoom(room)
                }
            }
        }
    }

    // @formatter:off - IDEA formatter really broke this method...
    private fun isRoomStale(room: GameRoom): Boolean =
        room.state == GameRoomState.INVALID
                || (room.state == GameRoomState.SETTING && Duration.between(room.created, Instant.now()) > GameService.ROOM_SETTING_TIMEOUT)
    // @formatter:on

    private fun disposeRoom(room: GameRoom) {
        synchronized(engagedUsers) {
            room.players.values.asSequence()
                .map(Player::user)
                .map(User::id)
                .forEach(engagedUsers::remove)
        }

        applicationEventPublisher.publishEvent(RoomRemovedEvent(room.toRoomInfo()))
    }

    private fun generateRoomId(): UUID {
        synchronized(rooms) {
            var uuid: UUID
            do {
                uuid = UUID.randomUUID()
            } while (rooms.containsKey(uuid))
            return uuid
        }
    }

    private fun changeRoomSettings(roomId: UUID, action: (GameRoom) -> Unit): GameRoomInfo {
        synchronized(rooms) {
            val room = rooms[roomId] ?: throw RoomNotFoundException(roomId)
            if (room.state !== GameRoomState.SETTING) {
                throw RoomStateException(GameRoomState.SETTING, room.state)
            }
            val oldInfo = room.toRoomInfo()
            room.unreadyPlayers()
            action(room)
            val currentInfo = room.toRoomInfo()
            applicationEventPublisher.publishEvent(
                RoomSettingsChangedEvent(
                    room = currentInfo,
                    oldRoom = oldInfo
                )
            )

            return currentInfo
        }
    }

    private fun logFinishedGame(room: GameRoom, endReason: GameEndReason) {
        synchronized(room) {
            if (room.state != GameRoomState.FINISHED) {
                throw RoomStateException(GameRoomState.FINISHED, room.state)
            }

            gameLogDao.create(
                name = room.name,
                created = room.created,
                finished = Instant.now(),
                players = room.participants,
                winners = room.winners,
                endReason = endReason
            )
        }
    }
}
