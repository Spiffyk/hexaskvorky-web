package cz.spiffyk.hexaskvorky.web.game.room

import cz.spiffyk.hexaskvorky.core.HexaskvorkyGame
import cz.spiffyk.hexaskvorky.core.HexaskvorkyGameLogic
import cz.spiffyk.hexaskvorky.core.exception.HexaskvorkyCoreException
import cz.spiffyk.hexaskvorky.core.vec.Point2i
import cz.spiffyk.hexaskvorky.web.game.exception.*
import cz.spiffyk.hexaskvorky.web.user.User
import java.time.Instant
import java.util.*

/**
 * A room of a Hexaskvorky game.
 */
class GameRoom(
    /** Room ID. */
    val id: UUID,

    /** The room creation timestamp. */
    val created: Instant,

    /** The room name. */
    var name: String,

    /** The owner of the room. */
    var owner: User,

    /** The room end score. */
    var endScore: Int,

    /** The size of the room - maximum number of players. */
    var maxPlayers: Int,

    /** Whether the room is private or public. */
    var visibility: GameRoomVisibility,
) {

    /**
     * Player slots mapped by their slot ID.
     */
    val players: MutableMap<UUID, Player> = mutableMapOf()

    lateinit var playerToInGame: MutableMap<UUID, Int>
    lateinit var inGameToPlayer: MutableMap<Int, UUID>

    var state: GameRoomState = GameRoomState.SETTING
        private set

    lateinit var game: HexaskvorkyGame

    lateinit var participants: List<User>

    val winners: List<User> get() = if (state === GameRoomState.FINISHED) {
        if (game.state === HexaskvorkyGame.State.FINISHED) {
            inGameToPlayer[game.winner]!!
                .let(players::get)!!
                .let(Player::user)
                .let(::listOf)
        } else {
            players.values.map(Player::user)
        }
    } else {
        emptyList()
    }

    @Synchronized
    fun start() {
        if (this.state != GameRoomState.SETTING) {
            throw RoomStateException(GameRoomState.SETTING, this.state)
        }

        participants = players.values.map(Player::user)

        // generate player maps
        val newPlayerToInGame = mutableMapOf<UUID, Int>()
        val newInGameToPlayer = mutableMapOf<Int, UUID>()
        var inGameId = 0
        for (uuid in players.keys) {
            newPlayerToInGame[uuid] = inGameId
            newInGameToPlayer[inGameId] = uuid
            inGameId += 1
        }
        playerToInGame = newPlayerToInGame
        inGameToPlayer = newInGameToPlayer

        // create game
        this.game = HexaskvorkyGameLogic(
            numPlayers = players.size,
            endScore = endScore
        )
        this.state = GameRoomState.IN_GAME
    }

    @Synchronized
    fun add(user: User, state: PlayerState): UUID {
        if (this.state != GameRoomState.SETTING) {
            throw RoomStateException(GameRoomState.SETTING, this.state)
        }
        if (this.players.size >= this.maxPlayers) {
            throw RoomFullException(this.toRoomInfo())
        }

        val player = Player(user, randomSlotId(), state)
        players[player.slotId] = player
        return player.slotId
    }

    @Synchronized
    fun removeBySlotId(slotId: UUID): UUID {
        val player = players[slotId] ?: throw PlayerNotFoundException.ofSlotId(this.id, slotId)
        players.remove(slotId)

        if (state != GameRoomState.SETTING) {
            playerToInGame.remove(slotId)
            inGameToPlayer.iterator().let {
                while (it.hasNext()) {
                    val containedSlotId = it.next().value
                    if (slotId == containedSlotId) {
                        it.remove()
                    }
                }
            }
        }
        afterPlayerRemoved(player.user.id)
        return player.user.id
    }

    @Synchronized
    fun removeByUserId(userId: UUID) {
        val it = players.iterator()
        var removed = false
        while (it.hasNext()) {
            val entry = it.next()
            if (entry.value.user.id == userId) {
                if (state != GameRoomState.SETTING) {
                    playerToInGame.remove(entry.value.slotId)
                    inGameToPlayer.iterator().let {
                        while (it.hasNext()) {
                            val containedSlotId = it.next().value
                            if (entry.value.slotId == containedSlotId) {
                                it.remove()
                            }
                        }
                    }
                }

                it.remove()
                removed = true
            }
        }
        if (!removed) {
            throw PlayerNotFoundException.ofUserId(this.id, userId)
        }

        afterPlayerRemoved(userId)
    }

    @Synchronized
    fun setPlayerStateBySlotId(slotId: UUID, state: PlayerState) {
        if (this.state != GameRoomState.SETTING) {
            throw RoomStateException(GameRoomState.SETTING, this.state)
        }
        val player = players[slotId] ?: throw PlayerNotFoundException.ofSlotId(this.id, slotId)
        player.state = state
        afterPlayerStateSet()
    }

    private fun afterPlayerRemoved(userId: UUID) {
        if (state == GameRoomState.IN_GAME) {
            state = GameRoomState.FINISHED
            return
        }

        if (players.isEmpty()) {
            state = GameRoomState.INVALID
            return
        }

        if (userId == owner.id) {
            owner = players.values.first().user
        }
    }

    private fun afterPlayerStateSet() {
        if (this.players.size == this.maxPlayers && areAllPlayersReady() && this.state == GameRoomState.SETTING) {
            this.start()
        }
    }

    private fun areAllPlayersReady(): Boolean {
        for ((_, player) in this.players) {
            if (player.state != PlayerState.READY) {
                return false
            }
        }
        return true
    }

    /**
     * Marks all [PlayerState.READY] players as [PlayerState.NOT_READY].
     */
    @Synchronized
    fun unreadyPlayers() {
        players.values.forEach { player ->
            if (player.state == PlayerState.READY) {
                player.state = PlayerState.NOT_READY
            }
        }
    }

    @Synchronized
    fun toRoomInfo(): GameRoomInfo =
        GameRoomInfo(
            id = id,
            created = created,
            name = name,
            owner = owner,
            maxPlayers = maxPlayers,
            players = players.values.toList(),
            state = state,
            visibility = visibility,
            endScore = endScore
        )

    @Synchronized
    fun toFieldInfo(): GameFieldInfo = if (state == GameRoomState.IN_GAME || state == GameRoomState.FINISHED) {
        GameFieldInfo(
            slotToInt = playerToInGame.toMap(),
            intToSlot = inGameToPlayer.toMap(),
            gameField = filledHexesSnapshot(),
            currentPlayer = game.currentPlayer,
            winners = when {
                (game.state == HexaskvorkyGame.State.FINISHED) -> listOf(game.winner)
                (state == GameRoomState.FINISHED) -> inGameToPlayer.keys.toList()
                else -> listOf()
            },
            winningPoints = when {
                (game.state == HexaskvorkyGame.State.FINISHED) -> game.winningPoints.toList()
                else -> listOf()
            }
        )
    } else {
        throw RoomStateException("Cannot get field info of room in state $state")
    }

    private fun filledHexesSnapshot(): Map<Point2i, Int> {
        val result = mutableMapOf<Point2i, Int>()
        game.forEachFilledHex(result::put)
        return result.toMap()
    }

    @Synchronized
    fun play(slotId: UUID, point: Point2i): Int {
        val inGamePlayer = this.playerToInGame[slotId]
            ?: throw PlayerNotFoundException.ofSlotId(this.id, slotId)
        if (this.state !== GameRoomState.IN_GAME) {
            throw RoomStateException(GameRoomState.IN_GAME, this.state)
        }
        if (this.game.currentPlayer != inGamePlayer) {
            throw NotPlayersTurnException(slotId)
        }

        try {
            this.game.play(point)
        } catch (e: HexaskvorkyCoreException) {
            throw GameException(cause = e)
        }

        if (this.game.state == HexaskvorkyGame.State.FINISHED) {
            this.state = GameRoomState.FINISHED
        }

        return inGamePlayer
    }

    private fun randomSlotId(): UUID {
        var uuid: UUID
        do {
            uuid = UUID.randomUUID()
        } while (players.containsKey(uuid))
        return uuid
    }
}
