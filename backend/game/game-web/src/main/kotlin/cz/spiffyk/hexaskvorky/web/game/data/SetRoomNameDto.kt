package cz.spiffyk.hexaskvorky.web.game.data

data class SetRoomNameDto(
    val name: String
)
