package cz.spiffyk.hexaskvorky.web.game

import cz.spiffyk.hexaskvorky.web.game.data.GameLogOutDto
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
@RequestMapping("/game-log")
@Tag(
    name = "Game Logs",
    description = "Operations for the game log: history of games, their participants and winners"
)
class GameLogController(
    val gameService: GameService
) {

    @GetMapping
    fun getToday(): List<GameLogOutDto> =
        gameService.getGameLogs().map(GameLogOutDto::ofGameLog)

    @GetMapping("/{date}")
    fun getForDate(@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") date: LocalDate): List<GameLogOutDto> =
        gameService.getGameLogs(date).map(GameLogOutDto::ofGameLog)

}
