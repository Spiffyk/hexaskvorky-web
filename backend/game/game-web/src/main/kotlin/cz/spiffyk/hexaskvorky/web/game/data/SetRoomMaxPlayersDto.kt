package cz.spiffyk.hexaskvorky.web.game.data

data class SetRoomMaxPlayersDto(
    val maxPlayers: Int
)
