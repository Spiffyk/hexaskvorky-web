package cz.spiffyk.hexaskvorky.web.game.data

import cz.spiffyk.hexaskvorky.web.user.web.common.data.UserOutDto

data class UserConnectionOutDto(val user: UserOutDto)
