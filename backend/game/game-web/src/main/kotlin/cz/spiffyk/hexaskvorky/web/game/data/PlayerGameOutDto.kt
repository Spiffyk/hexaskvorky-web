package cz.spiffyk.hexaskvorky.web.game.data

import cz.spiffyk.hexaskvorky.web.game.room.GameRoomInfo
import cz.spiffyk.hexaskvorky.web.user.web.common.data.UserOutDto

/**
 * DTO sent to players in a game.
 */
data class PlayerGameOutDto(
    val id: String,
    val name: String,
    val owner: UserOutDto,
    val maxPlayers: Int,
    val players: Collection<PlayerOutDto>,
    val state: String,
    val visibility: String,
    val endScore: Int
) {

    companion object {
        fun ofGameRoomInfo(room: GameRoomInfo): PlayerGameOutDto =
            PlayerGameOutDto(
                id = room.id.toString(),
                name = room.name,
                owner = UserOutDto.ofUser(room.owner),
                maxPlayers = room.maxPlayers,
                players = room.players
                    .map(PlayerOutDto::ofPlayer)
                    .toList(),
                state = room.state.name,
                visibility = room.visibility.name,
                endScore = room.endScore
            )
    }

}
