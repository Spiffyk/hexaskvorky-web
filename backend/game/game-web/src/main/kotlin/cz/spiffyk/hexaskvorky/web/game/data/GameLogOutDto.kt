package cz.spiffyk.hexaskvorky.web.game.data

import cz.spiffyk.hexaskvorky.web.game.room.GameLog
import cz.spiffyk.hexaskvorky.web.user.User

data class GameLogOutDto(
    val id: String,
    val name: String,
    val created: String,
    val finished: String,
    val players: List<String>,
    val winners: List<String>,
    val endReason: String
) {


    companion object {
        fun ofGameLog(log: GameLog): GameLogOutDto =
            GameLogOutDto(
                id = log.id.toString(),
                name = log.name,
                created = log.created.toString(),
                finished = log.finished.toString(),
                players = log.players.map(User::displayName),
                winners = log.winners.map(User::displayName),
                endReason = log.endReason.name
            )
    }
}
