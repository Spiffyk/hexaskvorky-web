package cz.spiffyk.hexaskvorky.web.game.data

import cz.spiffyk.hexaskvorky.web.user.web.common.data.UserOutDto

data class PlayerJoinOutDto(
    val type: Type,
    val game: PlayerGameOutDto,
    val inviter: UserOutDto?
) {

    enum class Type {
        INVITED, ADDED
    }

}
