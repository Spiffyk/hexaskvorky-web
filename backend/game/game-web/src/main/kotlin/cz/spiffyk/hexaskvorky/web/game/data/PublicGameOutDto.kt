package cz.spiffyk.hexaskvorky.web.game.data

import cz.spiffyk.hexaskvorky.web.game.room.GameRoomInfo

data class PublicGameOutDto(
    val id: String,
    val name: String,
    val maxPlayers: Int,
    val players: Int,
    val endScore: Int
) {

    companion object {
        fun ofGameRoomInfo(info: GameRoomInfo): PublicGameOutDto =
            PublicGameOutDto(
                id = info.id.toString(),
                name = info.name,
                maxPlayers = info.maxPlayers,
                players = info.players.size,
                endScore = info.endScore,
            )
    }

}
