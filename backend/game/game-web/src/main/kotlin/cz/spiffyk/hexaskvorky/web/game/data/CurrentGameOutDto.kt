package cz.spiffyk.hexaskvorky.web.game.data

data class CurrentGameOutDto(
    val game: PlayerGameOutDto?
) {

    val hasGame: Boolean
        get() = game !== null

}
