package cz.spiffyk.hexaskvorky.web.game.data

data class SetRoomVisibilityDto(
    val public: Boolean
)
