package cz.spiffyk.hexaskvorky.web.game.data

data class CurrentFieldOutDto(
    val field: GameFieldOutDto?
)
