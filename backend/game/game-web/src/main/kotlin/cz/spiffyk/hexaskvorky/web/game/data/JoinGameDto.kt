package cz.spiffyk.hexaskvorky.web.game.data

data class JoinGameDto(val roomId: String)
