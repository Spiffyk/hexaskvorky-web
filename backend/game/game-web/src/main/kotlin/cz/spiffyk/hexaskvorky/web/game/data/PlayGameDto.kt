package cz.spiffyk.hexaskvorky.web.game.data

data class PlayGameDto(
    val x: Int,
    val y: Int
)
