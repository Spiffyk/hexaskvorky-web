package cz.spiffyk.hexaskvorky.web.game.data

import cz.spiffyk.hexaskvorky.web.game.room.GameRoomInfo

data class RoomIdOutDto(
    val id: String
) {

    companion object {

        fun ofGameRoomInfo(room: GameRoomInfo): RoomIdOutDto =
            RoomIdOutDto(room.id.toString())

    }

}
