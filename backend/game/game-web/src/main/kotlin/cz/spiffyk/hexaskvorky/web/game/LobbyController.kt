package cz.spiffyk.hexaskvorky.web.game

import cz.spiffyk.hexaskvorky.web.common.StatusMessageException
import cz.spiffyk.hexaskvorky.web.game.data.*
import cz.spiffyk.hexaskvorky.web.game.event.*
import cz.spiffyk.hexaskvorky.web.game.exception.GameException
import cz.spiffyk.hexaskvorky.web.game.exception.RoomFullException
import cz.spiffyk.hexaskvorky.web.game.exception.UserEngagedException
import cz.spiffyk.hexaskvorky.web.game.exception.UserNotInvitedException
import cz.spiffyk.hexaskvorky.web.game.room.GameRoomVisibility
import cz.spiffyk.hexaskvorky.web.game.room.InviteDto
import cz.spiffyk.hexaskvorky.web.game.room.PlayerState
import cz.spiffyk.hexaskvorky.web.user.UserService
import cz.spiffyk.hexaskvorky.web.user.event.UserOfflineEvent
import cz.spiffyk.hexaskvorky.web.user.event.UserOnlineEvent
import cz.spiffyk.hexaskvorky.web.user.web.common.data.UserOutDto
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.context.event.EventListener
import org.springframework.http.HttpStatus
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/lobby")
@MessageMapping("/lobby")
@Tag(
    name = "Lobby",
    description = "Operations for the lobby: lists of games, invitations etc."
)
class LobbyController(
    val gameService: GameService,
    val userService: UserService,
    val messagingTemplate: SimpMessagingTemplate
) {

    @GetMapping("/games")
    @Operation(
        summary = "List public games",
        description = "Lists all currently active public game rooms."
    )
    fun listPublicGames(): List<PublicGameOutDto> =
        gameService.getPublicRooms().map(PublicGameOutDto::ofGameRoomInfo)

    @PostMapping("/create-game")
    @Operation(
        summary = "Create a game",
        description = "Creates a new game room with default settings."
    )
    fun createGame() {
        val currentUser = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        val room = gameService.createRoom(currentUser)
        gameService.addPlayerToRoom(room.id, currentUser, PlayerState.NOT_READY)
    }

    @PostMapping("/join-game")
    @Operation(
        summary = "Join a public game",
        description = "Joins the specified public game."
    )
    fun joinGame(@RequestBody dto: JoinGameDto) {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        val room = gameService.getRoomInfo(UUID.fromString(dto.roomId))
        if (room == null || room.visibility != GameRoomVisibility.PUBLIC) {
            throw StatusMessageException(HttpStatus.CONFLICT,
                "Room with ID ${dto.roomId} not found or not public")
        }

        try {
            gameService.addPlayerToRoom(room.id, user, PlayerState.NOT_READY)
        } catch (e: RoomFullException) {
            throw StatusMessageException(HttpStatus.CONFLICT, "Room is full", e)
        } catch (e: GameException) {
            throw StatusMessageException(HttpStatus.CONFLICT, "Room join rejected", e)
        }
    }

    @PostMapping("/invitation/create")
    @Operation(
        summary = "Invite a player to a game",
        description = "Invites the specified player to join a game room."
    )
    fun invite(@RequestBody dto: InviteDto) {
        val inviter = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        val room = gameService.getEngagedRoomInfo(inviter)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "You are not engaged in a room")
        val invitee = userService.getById(dto.inviteeId)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "User ${dto.inviteeId} does not exist")
        if (!userService.isOnline(invitee.id)) {
            throw StatusMessageException(HttpStatus.CONFLICT, "${invitee.displayName} is not online")
        }

        try {
            gameService.addPlayerToRoom(room.id, invitee, PlayerState.INVITED, inviter)
        } catch (e: RoomFullException) {
            throw StatusMessageException(HttpStatus.CONFLICT, "Room is full", e)
        } catch (e: UserEngagedException) {
            throw StatusMessageException(HttpStatus.CONFLICT, "${invitee.displayName} is already in a game", e)
        } catch (e: GameException) {
            throw StatusMessageException(HttpStatus.CONFLICT, "Invitation failed", e)
        }
    }

    @PostMapping("/invitation/accept")
    @Operation(summary = "Accept invitation")
    fun acceptInvitation() {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        try {
            val room = gameService.acceptInvitation(user)
            messagingTemplate.convertAndSendToUser(
                user.id.toString(),
                "/topic/lobby/join",
                PlayerJoinOutDto(
                    type = PlayerJoinOutDto.Type.ADDED,
                    game = PlayerGameOutDto.ofGameRoomInfo(room),
                    inviter = null
                )
            )
        } catch (e: UserNotInvitedException) {
            throw StatusMessageException(HttpStatus.CONFLICT, "Not invited into any room", e)
        }
    }


    @PostMapping("/invitation/reject")
    @Operation(summary = "Reject invitation")
    fun rejectInvitation() {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        try {
            gameService.rejectInvitation(user);
        } catch (e: UserNotInvitedException) {
            throw StatusMessageException(HttpStatus.CONFLICT, "Not invited into any room", e)
        }
    }


    @EventListener(RoomCreatedEvent::class)
    fun onRoomCreated(event: RoomCreatedEvent) {
        if (event.room.visibility == GameRoomVisibility.PUBLIC) {
            messagingTemplate.convertAndSend(
                "/topic/lobby/room/published",
                PublicGameOutDto.ofGameRoomInfo(event.room)
            )
        }
    }

    @EventListener(RoomRemovedEvent::class)
    fun onRoomRemoved(event: RoomRemovedEvent) {
        if (event.room.visibility == GameRoomVisibility.PUBLIC) {
            messagingTemplate.convertAndSend(
                "/topic/lobby/room/depublished",
                RoomIdOutDto.ofGameRoomInfo(event.room)
            )
        }
    }

    @EventListener(PlayerJoinEvent::class)
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val type = when (event.playerState) {
            PlayerState.INVITED -> PlayerJoinOutDto.Type.INVITED
            PlayerState.NOT_READY -> PlayerJoinOutDto.Type.ADDED
            else -> throw IllegalStateException("Cannot add player to a game with initial state ${event.playerState}")
        }

        messagingTemplate.convertAndSendToUser(
            event.user.id.toString(),
            "/topic/lobby/join",
            PlayerJoinOutDto(
                type = type,
                game = PlayerGameOutDto.ofGameRoomInfo(event.room),
                inviter = event.inviter?.let(UserOutDto::ofUser)
            )
        )
    }

    @EventListener(RoomSettingsChangedEvent::class)
    fun onRoomSettingsChanged(event: RoomSettingsChangedEvent) {
        if (event.room.visibility == GameRoomVisibility.PUBLIC) {
            messagingTemplate.convertAndSend(
                "/topic/lobby/room/published",
                PublicGameOutDto.ofGameRoomInfo(event.room)
            )
        } else if (event.oldRoom.visibility == GameRoomVisibility.PUBLIC && event.room.visibility == GameRoomVisibility.PRIVATE) {
            messagingTemplate.convertAndSend(
                "/topic/lobby/room/depublished",
                RoomIdOutDto.ofGameRoomInfo(event.room)
            )
        }
    }

    @EventListener(UserOnlineEvent::class)
    fun onUserOnline(event: UserOnlineEvent) {
        messagingTemplate.convertAndSend(
            "/topic/lobby/user/connect",
            UserConnectionOutDto(UserOutDto.ofUser(event.user))
        )
    }

    @EventListener(UserOfflineEvent::class)
    fun onUserOffline(event: UserOfflineEvent) {
        messagingTemplate.convertAndSend(
            "/topic/lobby/user/disconnect",
            UserConnectionOutDto(UserOutDto.ofUser(event.user))
        )
    }
}
