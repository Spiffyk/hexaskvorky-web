package cz.spiffyk.hexaskvorky.web.game.data

import cz.spiffyk.hexaskvorky.web.game.room.GameFieldInfo

data class GameFieldOutDto(
    val slotToInt: Map<String, Int>,
    val intToSlot: Map<String, String>,
    val gameField: Map<String, Int>,
    val currentPlayer: Int,
    val winners: List<Int>,
    val winningPoints: List<String>
) {

    companion object {
        fun ofGameFieldInfo(info: GameFieldInfo): GameFieldOutDto {
            val slotToIntConv = info.slotToInt.mapKeys { it.key.toString() }

            val intToSlotConvMut = mutableMapOf<String, String>()
            info.intToSlot.forEach { int, slot -> intToSlotConvMut[int.toString()] = slot.toString() }
            val intToSlotConv = intToSlotConvMut.toMap()

            val gameFieldConv = info.gameField.mapKeys { "${it.key.x} ${it.key.y}" }

            return GameFieldOutDto(
                slotToInt = slotToIntConv,
                intToSlot = intToSlotConv,
                gameField = gameFieldConv,
                currentPlayer = info.currentPlayer,
                winners = info.winners,
                winningPoints = info.winningPoints.map { "${it.x} ${it.y}" }
            )
        }
    }
}
