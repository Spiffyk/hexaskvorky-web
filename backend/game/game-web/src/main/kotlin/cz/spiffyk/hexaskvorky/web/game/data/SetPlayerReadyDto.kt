package cz.spiffyk.hexaskvorky.web.game.data

data class SetPlayerReadyDto(
    val ready: Boolean
)
