package cz.spiffyk.hexaskvorky.web.game.data

data class SetRoomEndScoreDto(
    val endScore: Int
)
