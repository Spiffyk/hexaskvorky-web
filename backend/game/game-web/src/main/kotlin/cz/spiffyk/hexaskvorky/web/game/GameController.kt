package cz.spiffyk.hexaskvorky.web.game

import cz.spiffyk.hexaskvorky.web.common.StatusMessageException
import cz.spiffyk.hexaskvorky.web.game.data.*
import cz.spiffyk.hexaskvorky.web.game.event.GamePlayedEvent
import cz.spiffyk.hexaskvorky.web.game.event.PlayerLeaveEvent
import cz.spiffyk.hexaskvorky.web.game.event.RoomRemovedEvent
import cz.spiffyk.hexaskvorky.web.game.event.RoomSettingsChangedEvent
import cz.spiffyk.hexaskvorky.web.game.room.GameRoomState
import cz.spiffyk.hexaskvorky.web.game.room.GameRoomVisibility
import cz.spiffyk.hexaskvorky.web.game.room.Player
import cz.spiffyk.hexaskvorky.web.game.room.PlayerState
import cz.spiffyk.hexaskvorky.web.user.User
import cz.spiffyk.hexaskvorky.web.user.UserService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.context.event.EventListener
import org.springframework.http.HttpStatus
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/game")
@MessageMapping("/game")
@Tag(
    name = "Game",
    description = "Operations for controlling of the game the user is currently engaged in."
)
class GameController(
    val gameService: GameService,
    val userService: UserService,
    val messagingTemplate: SimpMessagingTemplate
) {

    @PostMapping("/leave")
    @Operation(
        summary = "Leave game",
        description = "Disengages the user from the current game (if any)"
    )
    fun leave() {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        gameService.disengageUser(user)
    }

    @GetMapping("/current")
    @Operation(
        summary = "Current game info",
        description = "Gets data about the room the user is currently engaged in (if any)"
    )
    fun getCurrent(): CurrentGameOutDto {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        return CurrentGameOutDto(gameService.getEngagedRoomInfo(user)?.let {
            PlayerGameOutDto.ofGameRoomInfo(it)
        })
    }

    @GetMapping("/current-field")
    @Operation(
        summary = "Current game field info",
        description = "Gets data about the room the user is currently engaged in (if any) from the perspective of the " +
                "game. Includes information like filled hexes, winning hexes, winner players etc."
    )
    fun getCurrentField(): CurrentFieldOutDto {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        return CurrentFieldOutDto(gameService.getEngagedRoomFieldInfo(user)?.let {
            GameFieldOutDto.ofGameFieldInfo(it)
        })
    }

    @MessageMapping("/set-name")
    fun setRoomName(dto: SetRoomNameDto) {
        val name = if (dto.name.isBlank()) {
            "Unnamed room"
        } else {
            dto.name.trim()
        }
        roomSettings { roomId -> gameService.setRoomName(roomId, name) }
    }

    @MessageMapping("/set-max-players")
    fun setRoomMaxPlayers(dto: SetRoomMaxPlayersDto) {
        val maxPlayers = when {
            (dto.maxPlayers < GameService.MIN_ROOM_PLAYERS) -> GameService.MIN_ROOM_PLAYERS
            (dto.maxPlayers > GameService.MAX_ROOM_PLAYERS) -> GameService.MAX_ROOM_PLAYERS
            else -> dto.maxPlayers
        }
        roomSettings { roomId -> gameService.setRoomMaxPlayers(roomId, maxPlayers) }
    }

    @MessageMapping("/set-end-score")
    fun setRoomEndScore(dto: SetRoomEndScoreDto) {
        val endScore = when {
            (dto.endScore < GameService.MIN_END_SCORE) -> GameService.MIN_END_SCORE
            (dto.endScore > GameService.MAX_END_SCORE) -> GameService.MAX_END_SCORE
            else -> dto.endScore
        }
        roomSettings { roomId -> gameService.setRoomEndScore(roomId, endScore) }
    }

    @MessageMapping("/set-visibility")
    fun setRoomVisibility(dto: SetRoomVisibilityDto) {
        roomSettings { roomId ->
            gameService.setRoomVisibility(
                roomId,
                if (dto.public) {
                    GameRoomVisibility.PUBLIC
                } else {
                    GameRoomVisibility.PRIVATE
                }
            )
        }
    }

    @MessageMapping("/self/set-ready")
    fun setPlayerReady(dto: SetPlayerReadyDto) {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        val room = gameService.getEngagedRoomInfo(user)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "You are not in a room")
        gameService.setPlayerState(
            room.id, user, if (dto.ready) {
                PlayerState.READY
            } else {
                PlayerState.NOT_READY
            }
        )
    }

    @MessageMapping("/play")
    fun playGame(dto: PlayGameDto) {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        val room = gameService.getEngagedRoomInfo(user)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "You are not in a room")
        gameService.play(room.id, user, dto.x, dto.y)
    }

    private fun roomSettings(action: (UUID) -> Unit) {
        val user = userService.getCurrentUser()
            ?: throw StatusMessageException(HttpStatus.FORBIDDEN, "No user is currently logged in")
        val room = gameService.getEngagedRoomInfo(user)
            ?: throw StatusMessageException(HttpStatus.CONFLICT, "You are not in a room")
        if (room.owner.id != user.id) {
            throw StatusMessageException(HttpStatus.FORBIDDEN, "You are not the owner")
        }

        action(room.id)
    }

    @EventListener(RoomSettingsChangedEvent::class)
    fun onRoomSettingsChanged(event: RoomSettingsChangedEvent) {
        event.room.players.asSequence()
            .map(Player::user)
            .map(User::id)
            .distinct()
            .forEach { userId ->
                messagingTemplate.convertAndSendToUser(
                    userId.toString(),
                    "/topic/game/changed",
                    PlayerGameOutDto.ofGameRoomInfo(event.room)
                )
            }
    }

    @EventListener(GamePlayedEvent::class)
    fun onGamePlayed(event: GamePlayedEvent) {
        event.room.players.asSequence()
            .map(Player::user)
            .map(User::id)
            .distinct()
            .forEach { userId ->
                messagingTemplate.convertAndSendToUser(
                    userId.toString(),
                    "/topic/game/played",
                    GameFieldOutDto.ofGameFieldInfo(event.field)
                )
            }
    }

    @EventListener(PlayerLeaveEvent::class)
    fun onPlayerLeave(event: PlayerLeaveEvent) {
        messagingTemplate.convertAndSendToUser(
            event.user.id.toString(),
            "/topic/game/leave",
            emptyList<Any>()
        )

        if (event.room.state == GameRoomState.FINISHED && event.field !== null) {
            event.room.players.asSequence()
                .map(Player::user)
                .map(User::id)
                .distinct()
                .forEach { userId ->
                    messagingTemplate.convertAndSendToUser(
                        userId.toString(),
                        "/topic/game/played",
                        GameFieldOutDto.ofGameFieldInfo(event.field!!)
                    )
                }
        }
    }

    @EventListener(RoomRemovedEvent::class)
    fun onRoomRemoved(event: RoomRemovedEvent) {
        event.room.players.asSequence()
            .map(Player::user)
            .forEach { user ->
                messagingTemplate.convertAndSendToUser(
                    user.id.toString(),
                    "/topic/game/leave",
                    emptyList<Any>()
                )
            }
    }

}
