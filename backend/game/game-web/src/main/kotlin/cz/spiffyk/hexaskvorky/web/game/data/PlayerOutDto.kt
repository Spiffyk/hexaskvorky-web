package cz.spiffyk.hexaskvorky.web.game.data

import cz.spiffyk.hexaskvorky.web.game.room.Player
import cz.spiffyk.hexaskvorky.web.user.web.common.data.UserOutDto

data class PlayerOutDto(
    val slotId: String,
    val state: String,
    val user: UserOutDto
) {

    companion object {
        fun ofPlayer(player: Player): PlayerOutDto =
            PlayerOutDto(
                slotId = player.slotId.toString(),
                state = player.state.name,
                user = UserOutDto.ofUser(player.user)
            )
    }

}
