package cz.spiffyk.hexaskvorky.web.game

import cz.spiffyk.hexaskvorky.web.game.room.GameEndReason
import cz.spiffyk.hexaskvorky.web.game.room.GameLog
import cz.spiffyk.hexaskvorky.web.user.User
import java.time.Instant
import java.time.LocalDate

interface GameLogDao {

    fun create(
        name: String,
        created: Instant,
        finished: Instant,
        players: Collection<User>,
        winners: Collection<User>,
        endReason: GameEndReason
    ): GameLog

    fun getAllByCreatedDate(date: LocalDate): List<GameLog>

}
