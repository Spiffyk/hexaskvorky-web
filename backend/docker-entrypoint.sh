#!/bin/sh

if [ -z "$HS_JPA_URL" ]; then
    HS_JPA_URL="jdbc:postgresql://localhost:5432/hexaskvorky"
fi

if [ -z "$HS_JPA_USER" ]; then
    HS_JPA_USER="hexaskvorky"
fi

if [ -z "$HS_JPA_PASS" ]; then
    HS_JPA_PASS="password"
fi

if [ -z "$HS_JWT_SECRET" ]; then
    HS_JWT_SECRET="l6XV1UKsOeL7ZiQjgECxTryTJnVlOJfDHuq7IAAQoeA="
fi

if [ -z "$HS_ADM_AUTO" ]; then
  HS_ADM_AUTO="true"
fi

if [ -z "$HS_ADM_EMAIL" ]; then
  HS_ADM_EMAIL="admin@example.com"
fi

if [ -z "$HS_ADM_PASS" ]; then
  HS_ADM_PASS="password"
fi

java \
    -Dhexaskvorky.jpa.url\="$HS_JPA_URL"\
    -Dhexaskvorky.jpa.username\="$HS_JPA_USER"\
    -Dhexaskvorky.jpa.password\="$HS_JPA_PASS"\
    -Dhexaskvorky.security.jwt.secret\="$HS_JWT_SECRET"\
    -Dhexaskvorky.admin.default.autoCreate\="$HS_ADM_AUTO"\
    -Dhexaskvorky.admin.default.email\="$HS_ADM_EMAIL"\
    -Dhexaskvorky.admin.default.password\="$HS_ADM_PASS"\
    -jar hexaskvorky-boot.jar
